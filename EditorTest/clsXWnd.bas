#include Once "windows.bi"
#include Once "clsXWnd.bi"

'XC_API HWINDOW WINAPI XWnd_Create(int x, int y, int cx, int cy, const wchar_t* pTitle, HWND hWndParent = NULL, int XCStyle = window_style_default);
Function clsXWnd.Create(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal pTitle As LPCWSTR,ByVal hWndParent As HWND = NULL,ByVal XCStyle As Long = window_style_default) As HWINDOW
	If pLib Then   
        Dim pFunc As Function(ByVal As Long,ByVal As Long,ByVal As Long,ByVal As Long,ByVal As LPCWSTR,ByVal As HWND,ByVal As Long) As HWINDOW
        pFunc = DyLibSymbol(pLib,"XWnd_Create") 
        If pFunc Then 
            m_hWindow = pFunc(x,y,cx,cy,pTitle,hWndParent,XCStyle)
        Else
    		Print "XWnd_Create函数不存在"
        End If
	Else
    	Print "DLL不存在"
	End If
	Return m_hWindow
End Function

'XC_API BOOL WINAPI XWnd_ShowWindow(HWINDOW hWindow, int nCmdShow);
Function clsXWnd.ShowWindow(ByVal nCmdShow As Long) As BOOL
	If pLib Then   
        Dim pFunc As Function(ByVal As HWINDOW,ByVal As Long) As BOOL
        pFunc = DyLibSymbol(pLib,"XWnd_ShowWindow") 
        If pFunc Then 
            Return pFunc(m_hWindow,nCmdShow)
        Else
        	Print "XWnd_ShowWindow函数不存在"
        End If
	Else
		Print "DLL不存在"
    End If 
End Function

Property clsXWnd.hWndHandle() As HWINDOW
	Return m_hWindow
End Property



