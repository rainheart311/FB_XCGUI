#ifndef __CLSXCGUI_BI__
#define __CLSXCGUI_BI__
#include Once "XCGUI.bi"

Type clsXCGUI Extends Object  
Public:
	Declare Function XInitXCGUI(ByVal bD2D As BOOL) As BOOL'XC_API BOOL WINAPI XInitXCGUI(BOOL bD2D);
	Declare Sub XRunXCGUI()'XC_API void WINAPI XRunXCGUI();
	Declare Sub XExitXCGUI()'XC_API void WINAPI XExitXCGUI();   
Public: 
	Declare Function UnicodeToAnsi(ByVal pIn As LPCWSTR,ByVal pOut As ZString Ptr,ByVal outLen As Long) As Long'XC_API int WINAPI XC_UnicodeToAnsi(const wchar_t* pIn, int inLen, out_ char* pOut, int outLen);
	Declare Function AnsiToUnicode(ByVal pIn As LPCSTR,ByVal pOut As WString Ptr,ByVal outLen As Long) As Long'XC_API int WINAPI XC_AnsiToUnicode(const char* pIn, int inLen, out_ wchar_t* pOut, int outLen);

	Declare Function SendMessage(ByVal hWin As HWINDOW,ByVal nMsg As UINT,ByVal wParam As WPARAM,ByVal lParam As LPARAM) As LRESULT'XC_API LRESULT WINAPI XC_SendMessage(HWINDOW hWindow, UINT msg, WPARAM wParam, LPARAM lParam);
	Declare Function PostMessage(ByVal hWin As HWINDOW,ByVal nMsg As UINT,ByVal wParam As WPARAM,ByVal lParam As LPARAM) As BOOL'XC_API BOOL    WINAPI XC_PostMessage(HWINDOW hWindow, UINT msg, WPARAM wParam, LPARAM lParam);
	Declare Function CallUiThread(ByVal pCall As funCallUiThread,ByVal nData As Integer) As Integer'XC_API vint WINAPI XC_CallUiThread(funCallUiThread pCall, vint data);
	Declare Sub DebugToFileInfo(ByVal pInfo As LPCSTR)'XC_API void WINAPI XC_DebugToFileInfo(const char* pInfo);//打印调试信息到文件 xcgui_debug.txt
	Declare Function IsHELE(ByVal hEle As HXCGUI) As BOOL'XC_API BOOL WINAPI XC_IsHELE(HXCGUI hEle); //检查句柄
	Declare Function IsHWINDOW(ByVal hWin As HXCGUI) As BOOL'XC_API BOOL WINAPI XC_IsHWINDOW(HXCGUI hWindow); //检查句柄
	Declare Function IsShape(ByVal hShape As HXCGUI) As BOOL'XC_API BOOL WINAPI XC_IsShape(HXCGUI hShape); //检查句柄
	Declare Function IsHXCGUI(ByVal hXCGUI As HXCGUI,ByVal nType As XC_OBJECT_TYPE) As BOOL'XC_API BOOL WINAPI XC_IsHXCGUI(HXCGUI hXCGUI, XC_OBJECT_TYPE nType);
	Declare Function hWindowFromHWnd(ByVal hWin As HWND) As HWINDOW'XC_API HWINDOW WINAPI XC_hWindowFromHWnd(HWND hWnd);
	Declare Function SetActivateTopWindow() As BOOL'XC_API BOOL    WINAPI XC_SetActivateTopWindow();
	Declare Function SetProperty(ByVal hXCGUI As HXCGUI,ByVal pName As LPCWSTR,ByVal pValue As LPCWSTR) As BOOL'XC_API BOOL WINAPI XC_SetProperty(HXCGUI hXCGUI, const wchar_t* pName, const wchar_t* pValue);
	Declare Function GetProperty(ByVal hXCGUI As HXCGUI,ByVal pName As LPCWSTR) As LPCWSTR'XC_API const wchar_t* WINAPI XC_GetProperty(HXCGUI hXCGUI, const wchar_t* pName);
	Declare Function RegisterWindowClassName(ByVal pClassName As LPCWSTR) As BOOL'XC_API BOOL WINAPI XC_RegisterWindowClassName(const wchar_t* pClassName); //注册窗口类名
	Declare Function IsSViewExtend(ByVal hEle As HELE) As BOOL'XC_API BOOL WINAPI XC_IsSViewExtend(HELE hEle);  //判断元素是否从滚动视图元素扩展的新元素,包含滚动视图元素
	Declare Function GetObjectType(ByVal hXCGUI As HXCGUI) As XC_OBJECT_TYPE'XC_API XC_OBJECT_TYPE WINAPI XC_GetObjectType(HXCGUI hXCGUI);
	Declare Function GetObjectByID(ByVal hWin As HWINDOW,ByVal nID As Long) As HXCGUI'XC_API HXCGUI WINAPI XC_GetObjectByID(HWINDOW hWindow, int nID); //通过ID获取对象句柄
	Declare Function GetObjectByIDName(ByVal hWin As HWINDOW,ByVal pName As LPCWSTR) As HXCGUI'XC_API HXCGUI WINAPI XC_GetObjectByIDName(HWINDOW hWindow, const wchar_t* pName);
	Declare Function GetObjectByUID(ByVal nUID As Long) As HXCGUI'XC_API HXCGUI WINAPI XC_GetObjectByUID(int nUID);
	Declare Function GetObjectByUIDName(ByVal pName As LPCWSTR) As HXCGUI'XC_API HXCGUI WINAPI XC_GetObjectByUIDName(const wchar_t* pName);
	Declare Function GetObjectByName(ByVal pName As LPCWSTR) As HXCGUI'XC_API HXCGUI WINAPI XC_GetObjectByName(const wchar_t* pName);
	Declare Sub SetPaintFrequency(ByVal nMilliseconds As Long)'XC_API void WINAPI XC_SetPaintFrequency(int nMilliseconds); //设置UI绘制频率
	Declare Sub SetTextRenderingHint(ByVal nType As Long)'XC_API void WINAPI XC_SetTextRenderingHint(int  nType);   //设置文本渲染质量
	Declare Sub EnableGdiDrawText(ByVal bEnable As BOOL)'XC_API void WINAPI XC_EnableGdiDrawText(BOOL bEnable);
	Declare Function RectInRect(ByVal pRect1 As RECT Ptr,ByVal pRect2 As RECT Ptr) As BOOL'XC_API BOOL WINAPI XC_RectInRect(RECT* pRect1, RECT* pRect2);
	Declare Sub CombineRect(ByVal pDest As RECT Ptr,ByVal pSrc1 As RECT Ptr,ByVal pSrc2 As RECT Ptr)'XC_API void WINAPI XC_CombineRect(RECT* pDest, RECT* pSrc1, RECT* pSrc2);
	Declare Sub ShowLayoutFrame(ByVal bShow As BOOL)'XC_API void WINAPI XC_ShowLayoutFrame(BOOL bShow);
	Declare Sub EnableDebugFile(ByVal bEnable As BOOL)'XC_API void WINAPI XC_EnableDebugFile(BOOL bEnable);
	Declare Sub EnableResMonitor(ByVal bEnable As BOOL)'XC_API void WINAPI XC_EnableResMonitor(BOOL bEnable);
	Declare Sub SetLayoutFrameColor(ByVal nColor As COLORREF)'XC_API void WINAPI XC_SetLayoutFrameColor(COLORREF color);
	Declare Sub EnableErrorMessageBox(ByVal bEnable As BOOL)'XC_API void WINAPI XC_EnableErrorMessageBox(BOOL bEnabel);
	Declare Sub EnableAutoExitApp(ByVal bEnable As BOOL)'XC_API void WINAPI XC_EnableAutoExitApp(BOOL bEnabel);
	Declare Function LoadResource(ByVal pFileName As LPCWSTR) As BOOL'XC_API BOOL WINAPI XC_LoadResource(const wchar_t* pFileName);
	Declare Function LoadResourceZip(ByVal pZipFileName As LPCWSTR,ByVal pFileName As LPCWSTR,ByVal pPassword As LPCWSTR = NULL) As BOOL'XC_API BOOL WINAPI XC_LoadResourceZip(const wchar_t* pZipFileName, const wchar_t* pFileName, const wchar_t* pPassword = NULL);
	Declare Function LoadResourceZipMem(ByVal lpData As Any Ptr,ByVal length As Long,ByVal pFileName As LPCWSTR,ByVal pPassword As LPCWSTR = NULL) As BOOL'XC_API BOOL WINAPI XC_LoadResourceZipMem(void* data, int length, const wchar_t* pFileName, const wchar_t* pPassword = NULL);
	Declare Function LoadResourceFromString(ByVal pStringXML As ZString Ptr,ByVal pFileName As LPCWSTR) As BOOL'XC_API BOOL WINAPI XC_LoadResourceFromString(const char* pStringXML, const wchar_t* pFileName);
	Declare Function LoadResourceFromStringUtf8(ByVal pStringXML As ZString Ptr,ByVal pFileName As LPCWSTR) As BOOL'XC_API BOOL WINAPI XC_LoadResourceFromStringUtf8(const char* pStringXML, const wchar_t* pFileName);
	Declare Function LoadStyle(ByVal pFileName As LPCWSTR) As BOOL'XC_API BOOL WINAPI XC_LoadStyle(const wchar_t* pFileName);
	Declare Function LoadStyleZip(ByVal pZipFile As LPCWSTR,ByVal pFileName As LPCWSTR,ByVal pPassword As LPCWSTR = NULL) As BOOL'XC_API BOOL WINAPI XC_LoadStyleZip(const wchar_t* pZipFile, const wchar_t* pFileName, const wchar_t* pPassword = NULL);
	Declare Function LoadStyleZipMem(ByVal lpData As Any Ptr,ByVal length As Long,ByVal pFileName As LPCWSTR,ByVal pPassword As LPCWSTR = NULL) As BOOL'XC_API BOOL WINAPI XC_LoadStyleZipMem(void* data, int length, const wchar_t* pFileName, const wchar_t* pPassword = NULL);
	Declare Sub GetTextSize(ByVal pString As LPCWSTR,ByVal length As Long,ByVal hFontX As HFONTX,ByVal pOutSize As SIZE Ptr)'XC_API void WINAPI XC_GetTextSize(const wchar_t* pString, int length, HFONTX hFontX, out_ SIZE* pOutSize);
	Declare Sub GetTextShowSize(ByVal pString As LPCWSTR,ByVal length As Long,ByVal hFontX As HFONTX,ByVal pOutSize As SIZE Ptr)'XC_API void WINAPI XC_GetTextShowSize(const wchar_t* pString, int length, HFONTX hFontX, out_ SIZE* pOutSize);
	Declare Sub GetTextShowSizeEx(ByVal pString As LPCWSTR,ByVal length As Long,ByVal hFontX As HFONTX,ByVal nTextAlign As Long,ByVal pOutSize As SIZE Ptr)'XC_API void WINAPI XC_GetTextShowSizeEx(const wchar_t* pString, int length, HFONTX hFontX, int nTextAlign, out_ SIZE* pOutSize);
	'//XC_API void WINAPI XC_GetTextShowRect(const wchar_t* pString, int length, HFONTX hFontX, int width, out_ SIZE* pOutSize);
	Declare Function GetDefaultFont() As HFONTX'XC_API HFONTX WINAPI XC_GetDefaultFont(); //获取默认字体
	Declare Sub SetDefaultFont(ByVal hFontX As HFONTX)'XC_API void   WINAPI XC_SetDefaultFont(HFONTX hFontX); //设置默认字体
	Declare Sub AddFileSearchPath(ByVal pPath As LPCWSTR)'XC_API void  WINAPI XC_AddFileSearchPath(const wchar_t* pPath);
	Declare Sub InitFont(ByVal pFont As LOGFONTW Ptr,ByVal pName As LPCWSTR,ByVal nSize As Long,ByVal bBold As BOOL = FALSE,ByVal bItalic As BOOL = FALSE,ByVal bUnderline As BOOL = FALSE,ByVal bStrikeOut As BOOL = FALSE)'XC_API void  WINAPI XC_InitFont(LOGFONTW* pFont, wchar_t* pName, int size, BOOL bBold = FALSE, BOOL bItalic = FALSE, BOOL bUnderline = FALSE, BOOL bStrikeOut = FALSE);
	Declare Function Malloc(ByVal nSize As Long) As Any Ptr'XC_API  void* WINAPI XC_Malloc(int size);
	Declare Sub Free_(ByVal p As Any Ptr)'XC_API  void  WINAPI XC_Free(void* p);
	Declare Sub Alert(ByVal pTitle As LPCWSTR,ByVal pText As LPCWSTR)'XC_API void WINAPI  XC_Alert(const wchar_t* pTitle, const wchar_t* pText);
	Declare Function ShellExecute(ByVal hWin As HWND,ByVal lpOperation As LPCWSTR,ByVal lpTitle As LPCWSTR,ByVal lpParameters As LPCWSTR,ByVal lpDirectory As Const LPCWSTR,ByVal nShowCmd As Long) As HINSTANCE'XC_API HINSTANCE WINAPI XC_Sys_ShellExecute(HWND hwnd, const wchar_t* lpOperation, const wchar_t* lpFile, const wchar_t* lpParameters, const wchar_t* lpDirectory, int nShowCmd);
	Declare Function LoadLibrary(ByVal lpFileName As LPCWSTR) As HMODULE'XC_API HMODULE WINAPI XC_LoadLibrary(const wchar_t* lpFileName);
	Declare Function GetProcAddress(ByVal hModule As HMODULE,ByVal lpProcName As LPCSTR) As FARPROC'XC_API FARPROC WINAPI XC_GetProcAddress(HMODULE hModule, const char* lpProcName);
	Declare Function FreeLibrary(ByVal hModule As HMODULE) As BOOL'XC_API BOOL    WINAPI XC_FreeLibrary(HMODULE hModule);
	Declare Function LoadDll(ByVal pDllFileName As LPCWSTR) As HMODULE'XC_API HMODULE WINAPI XC_LoadDll(const wchar_t* pDllFileName);
	Declare Sub PostQuitMessage(ByVal nExitCode As Long)'XC_API void WINAPI XC_PostQuitMessage(int nExitCode); 
	Declare Sub RegJsBind(ByVal pName As LPCSTR,ByVal func As Long)'XC_API  void WINAPI _XC_RegJsBind(const char* pName, int func);
	Declare Sub RegFunExit(ByVal func As funExit)'XC_API  void WINAPI XC_RegFunExit(funExit func); 
'//v3.1.1
	Declare Function GetD2dFactory() As Integer'XC_API vint WINAPI XC_GetD2dFactory();
	Declare Function GetWicFactory() As Integer'XC_API vint WINAPI XC_GetWicFactory();
	Declare Function GetDWriteFactory() As Integer'XC_API vint WINAPI XC_GetDWriteFactory();
	Declare Sub SetD2dTextRenderingMode(ByVal nMode As XC_DWRITE_RENDERING_MODE)'XC_API void WINAPI XC_SetD2dTextRenderingMode(XC_DWRITE_RENDERING_MODE mode);	
	Declare Function IsEnableD2D() As BOOL'XC_API BOOL WINAPI XC_IsEnableD2D();
	Declare Function MessageBox(ByVal pTitle As LPCWSTR,ByVal pText As LPCWSTR,ByVal nFlags As DWORD = messageBox_flag_ok Or messageBox_flag_icon_info,ByVal hWndParent As HWND = NULL,ByVal XCStyle As Long = window_style_modal) As Long'XC_API int WINAPI XC_MessageBox(const wchar_t* pTitle, const wchar_t* pText, int nFlags=messageBox_flag_ok | messageBox_flag_icon_info, HWND hWndParent =0, int XCStyle = window_style_modal);
'//3.2.0
	Declare Function LoadStyleFromStringW(ByVal pString As LPCWSTR,ByVal pFileName As LPCWSTR) As BOOL'XC_API BOOL WINAPI XC_LoadStyleFromStringW(const wchar_t* pString, const wchar_t* pFileName);
	Declare Function LoadStyleFromString(ByVal pString As LPCSTR,ByVal pFileName As LPCWSTR) As BOOL'XC_API BOOL WINAPI XC_LoadStyleFromString(const char* pString, const wchar_t* pFileName);
	Declare Function LoadStyleFromStringUtf8(ByVal pString As LPCSTR,ByVal pFileName As LPCWSTR) As BOOL'XC_API BOOL WINAPI XC_LoadStyleFromStringUtf8(const char* pString, const wchar_t* pFileName);
	Declare Sub ShowSvgFrame(ByVal bShow As BOOL)'XC_API void WINAPI XC_ShowSvgFrame(BOOL bShow);
'//v3.3.2
	Declare Function LoadLayout(ByVal pFileName As LPCWSTR,ByVal hParent As HXCGUI = NULL,ByVal hAttachWnd As HWND = NULL) As HXCGUI'//增加参数 hAttachWnd'XC_API HXCGUI WINAPI XC_LoadLayout(const wchar_t *pFileName, HXCGUI hParent=NULL, HWND hAttachWnd=NULL);
	Declare Function LoadLayoutZip(ByVal pZipFileName As LPCWSTR,ByVal pFileName As LPCWSTR,ByVal pPassword As LPCWSTR = NULL,ByVal hParent As HXCGUI = NULL,ByVal hAttachWnd As HWND = NULL) As HXCGUI'XC_API HXCGUI WINAPI XC_LoadLayoutZip(const wchar_t *pZipFileName, const wchar_t *pFileName, const wchar_t* pPassword=NULL, HXCGUI hParent=NULL, HWND hAttachWnd = NULL);
	Declare Function LoadLayoutZipMem(ByVal lpData As Any Ptr,ByVal pFileName As LPCWSTR,ByVal pPassword As LPCWSTR = NULL,ByVal hParent As HXCGUI = NULL,ByVal hAttachWnd As HWND = NULL) As HXCGUI'XC_API HXCGUI WINAPI XC_LoadLayoutZipMem(void* data, int length, const wchar_t *pFileName, const wchar_t* pPassword = NULL, HXCGUI hParent = NULL, HWND hAttachWnd = NULL);
	Declare Function LoadLayoutFromString(ByVal pStringXML As LPCSTR,ByVal hParent As HXCGUI = NULL,ByVal hAttachWnd As HWND = NULL) As HXCGUI'XC_API HXCGUI WINAPI XC_LoadLayoutFromString(const char *pStringXML, HXCGUI hParent=NULL, HWND hAttachWnd = NULL);
	Declare Function LoadLayoutFromStringUtf8(ByVal pStringXML As LPCSTR,ByVal hParent As HXCGUI = NULL,ByVal hAttachWnd As HWND = NULL) As HXCGUI'XC_API HXCGUI WINAPI XC_LoadLayoutFromStringUtf8(const char *pStringXML, HXCGUI hParent = NULL, HWND hAttachWnd = NULL);
'//v3.3.4
	Declare Sub GetTextShowRect(ByVal pString As LPCWSTR,ByVal length As Long,ByVal hFontX As HFONTX,ByVal nTextAlign As Long,ByVal nWidth As Long,ByVal pOutSize As SIZE Ptr)'XC_API void WINAPI XC_GetTextShowRect(const wchar_t* pString, int length, HFONTX hFontX, int nTextAlign, int width, out_ SIZE* pOutSize);
Public:
	Declare Sub SetType(ByVal hXCGUI As HXCGUI,ByVal nType As XC_OBJECT_TYPE)'XC_API void WINAPI _XC_SetType(HXCGUI hXCGUI, XC_OBJECT_TYPE nType);
	Declare Sub AddType(ByVal hXCGUI As HXCGUI,ByVal nType As XC_OBJECT_TYPE)'XC_API void WINAPI _XC_AddType(HXCGUI hXCGUI, XC_OBJECT_TYPE nType);
	Declare Sub BindData(ByVal hXCGUI As HXCGUI,ByVal nData As Integer)'XC_API void WINAPI _XC_BindData(HXCGUI hXCGUI, vint data);
	Declare Function GetBindData(ByVal hXCGUI As HXCGUI) As Integer'XC_API vint WINAPI _XC_GetBindData(HXCGUI hXCGUI);
Public:	
'XC_API const char* WINAPI XC_itoa(int nValue);
'XC_API const wchar_t* WINAPI XC_itow(int nValue);
'XC_API const wchar_t* WINAPI XC_i64tow(__int64 nValue);
'XC_API const char* WINAPI XC_ftoa(float fValue);
'XC_API const wchar_t* WINAPI XC_ftow(float fValue);
'XC_API const wchar_t* WINAPI XC_fftow(double fValue);
'XC_API const wchar_t* WINAPI XC_atow(const char* pValue);
'XC_API const char* WINAPI XC_wtoa(const wchar_t* pValue);
'XC_API const wchar_t* WINAPI XC_utf8tow(const char* pUtf8);
'XC_API const wchar_t* WINAPI XC_utf8towEx(const char* pUtf8, int length);
'XC_API const char* WINAPI XC_utf8toa(const char* pUtf8);
'XC_API const char* WINAPI XC_atoutf8(const char* pValue);
'XC_API const char* WINAPI XC_wtoutf8(const wchar_t* pValue);
'XC_API const char* WINAPI XC_wtoutf8Ex(const wchar_t* pValue, int length);
End Type

Type clsXObj Extends Object 
Public:
	Declare Function GetType(ByVal hXCGUI As HXCGUI) As XC_OBJECT_TYPE'XC_API XC_OBJECT_TYPE    WINAPI XObj_GetType(HXCGUI hXCGUI);
	Declare Function GetTypeBase(ByVal hXCGUI As HXCGUI) As XC_OBJECT_TYPE'XC_API XC_OBJECT_TYPE    WINAPI XObj_GetTypeBase(HXCGUI hXCGUI);
	Declare Function GetTypeEx(ByVal hXCGUI As HXCGUI) As XC_OBJECT_TYPE_EX'XC_API XC_OBJECT_TYPE_EX WINAPI XObj_GetTypeEx(HXCGUI hXCGUI);	
	Declare Sub SetTypeEx(ByVal hXCGUI As HXCGUI,ByVal nType As XC_OBJECT_TYPE_EX)'XC_API void WINAPI XObj_SetTypeEx(HXCGUI hXCGUI, XC_OBJECT_TYPE_EX nType);
End Type

Type clsXUI Extends Object     
Public:
	Declare Sub SetStyle(ByVal hXCGUI As HXCGUI,ByVal nStyle As XC_OBJECT_STYLE)'XC_API void              WINAPI XUI_SetStyle(HXCGUI hXCGUI, XC_OBJECT_STYLE nStyle);
	Declare Function GetStyle(ByVal hXCGUI As HXCGUI) As XC_OBJECT_STYLE'XC_API XC_OBJECT_STYLE   WINAPI XUI_GetStyle(HXCGUI hXCGUI);
	Declare Sub EnableCSS(ByVal hXCGUI As HXCGUI,ByVal bEnable As BOOL)'XC_API void WINAPI XUI_EnableCSS(HXCGUI hXCGUI, BOOL bEnable);
	Declare Sub SetCssName(ByVal hXCGUI As HXCGUI,ByVal pName As LPCWSTR)'XC_API void WINAPI XUI_SetCssName(HXCGUI hXCGUI, const wchar_t* pName);
	Declare Function GetCssName(ByVal hXCGUI As HXCGUI) As LPCWSTR'XC_API const wchar_t* WINAPI XUI_GetCssName(HXCGUI hXCGUI);	
End Type

Type clsXDebug Extends Object 
Public: 
	Declare Sub Print(ByVal level As Long,ByVal pInfo As LPCSTR)'XC_API void WINAPI XDebug_Print(int  level, const char* pInfo);
	Declare Sub _xtrace Cdecl(ByVal pFormat As LPCSTR,...)'XC_API void WINAPI _xtrace(const char* pFormat, ...); //支持多线程
	Declare Sub _xtracew Cdecl(ByVal pFormat As LPCWSTR,...)'XC_API void WINAPI _xtracew(const wchar_t* pFormat, ...); //支持多线程
	Declare Sub OutputDebugStringA(ByVal pString As LPCSTR)'XC_API void WINAPI XDebug_OutputDebugStringA(const char* pString); //OutputDebugStringA
	Declare Sub OutputDebugStringW(ByVal pString As LPCWSTR)'XC_API void WINAPI XDebug_OutputDebugStringW(const wchar_t* pString); //OutputDebugStringW
	Declare Sub OutputDebugString_UTF8(ByVal bUTF8 As BOOL)'XC_API void WINAPI XDebug_Set_OutputDebugString_UTF8(BOOL bUTF8); //设置debug输出编码方式 encoding_utf8	
End Type

Type clsXEase Extends Object 
Public: 
	Declare Function Linear(ByVal p As Single) As Single'XC_API float WINAPI XEase_Linear(float p);
	Declare Function Quad(ByVal p As Single,ByVal nFlag As ease_type_) As Single'XC_API float WINAPI XEase_Quad(float p, ease_type_ flag);
	Declare Function Cubic(ByVal p As Single,ByVal nFlag As ease_type_) As Single'XC_API float WINAPI XEase_Cubic(float p, ease_type_ flag);
	Declare Function Quart(ByVal p As Single,ByVal nFlag As ease_type_) As Single'XC_API float WINAPI XEase_Quart(float p, ease_type_ flag);
	Declare Function Quint(ByVal p As Single,ByVal nFlag As ease_type_) As Single'XC_API float WINAPI XEase_Quint(float p, ease_type_ flag);
	Declare Function Sine(ByVal p As Single,ByVal nFlag As ease_type_) As Single'XC_API float WINAPI XEase_Sine(float p, ease_type_ flag);
	Declare Function Expo(ByVal p As Single,ByVal nFlag As ease_type_) As Single'XC_API float WINAPI XEase_Expo(float p, ease_type_ flag);
	Declare Function Circ(ByVal p As Single,ByVal nFlag As ease_type_) As Single'XC_API float WINAPI XEase_Circ(float p, ease_type_ flag);
	Declare Function Elastic(ByVal p As Single,ByVal nFlag As ease_type_) As Single'XC_API float WINAPI XEase_Elastic(float p, ease_type_ flag);
	Declare Function Back(ByVal p As Single,ByVal nFlag As ease_type_) As Single'XC_API float WINAPI XEase_Back(float p, ease_type_ flag);
	Declare Function Bounce(ByVal p As Single,ByVal nFlag As ease_type_) As Single'XC_API float WINAPI XEase_Bounce(float p, ease_type_ flag);
'//3.3.0
	Declare Function Ex(ByVal nPos As Single,ByVal nFlag As Long) As Single'XC_API float WINAPI XEase_Ex(float pos, int flag);	
End Type

#endif
