#include Once "windows.bi"
#include Once "clsXCGUI.bi"

Dim Shared pLib As Any Ptr

'================================================================================================================================================================================
'{ //////////////////////////////////////////////////////////////////////////////clsXCGUI Start////////////////////////////////////////////////////////////////////////////////////
'XC_API BOOL WINAPI XInitXCGUI(BOOL bD2D);
Function clsXCGUI.XInitXCGUI(ByVal bD2D As BOOL) As BOOL 
    If pLib Then   
        Dim pFunc As Function(ByVal bD2D As BOOL) As BOOL
        pFunc = DyLibSymbol(pLib,"XInitXCGUI") 
        If pFunc Then 
            Return pFunc(bD2D)
        Else
        	Print "XInitXCGUI函数不存在"
        End If
    Else
    	Print "DLL不存在"
    End If
End Function

'XC_API void WINAPI XRunXCGUI();
Sub clsXCGUI.XRunXCGUI()
    If pLib Then   
        Dim pFunc As Sub()
        pFunc = DyLibSymbol(pLib,"XRunXCGUI") 
        If pFunc Then 
            pFunc()
        Else
        	Print "XRunXCGUI函数不存在"
        End If
    Else
    	Print "DLL不存在"
    End If 
End Sub

'XC_API void WINAPI XExitXCGUI();   
Sub clsXCGUI.XExitXCGUI() '
    If pLib Then   
        Dim pFunc As Sub()
        pFunc = DyLibSymbol(pLib,"XExitXCGUI") 
        If pFunc Then 
            pFunc()
        Else
        	Print "XExitXCGUI函数不存在"
        End If
    Else
    	Print "DLL不存在"
    End If 
End Sub



