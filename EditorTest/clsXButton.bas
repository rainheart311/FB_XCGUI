#include Once "windows.bi"
#include Once "clsXButton.bi"

'XC_API HELE WINAPI XBtn_Create(int x, int y, int cx, int cy, const wchar_t* pName, HXCGUI hParent = NULL);
Function clsXButton.Create(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal pName As LPCWSTR,ByVal hParent As HXCGUI = NULL) As HELE
	If pLib Then   
        Dim pFunc As Function(ByVal As Long,ByVal As Long,ByVal As Long,ByVal As Long,ByVal As LPCWSTR,ByVal As HXCGUI) As HELE
        pFunc = DyLibSymbol(pLib,"XBtn_Create") 
        If pFunc Then 
            m_hEle = pFunc(x,y,cx,cy,pName,hParent)
        Else
    		Print "XBtn_Create函数不存在"
        End If
	Else
    	Print "DLL不存在"
	End If
	Return m_hEle
End Function

'XC_API void WINAPI XBtn_SetTypeEx(HELE hEle, XC_OBJECT_TYPE_EX nType);
Sub clsXButton.SetTypeEx(ByVal nType As XC_OBJECT_TYPE_EX)
	If pLib Then   
        Dim pFunc As Sub(ByVal As HELE,ByVal As XC_OBJECT_TYPE_EX)
        pFunc = DyLibSymbol(pLib,"XBtn_SetTypeEx") 
        If pFunc Then 
            pFunc(m_hEle,nType)
        Else
    		Print "XBtn_SetTypeEx函数不存在"
        End If
	Else
    	Print "DLL不存在"
    End If  
End Sub