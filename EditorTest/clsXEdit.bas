#include Once "windows.bi"
#include Once "clsXEdit.bi"

'XC_API HELE WINAPI XEdit_Create(int x, int y, int cx, int cy, HXCGUI hParent = NULL);
Function clsXEdit.Create(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal hParent As HXCGUI = NULL) As HELE
	If pLib Then   
        Dim pFunc As Function(ByVal As Long,ByVal As Long,ByVal As Long,ByVal As Long,ByVal As HXCGUI) As HELE
        pFunc = DyLibSymbol(pLib,"XEdit_Create") 
        If pFunc Then 
            m_hEle = pFunc(x,y,cx,cy,hParent)
        Else
    		Print "XEdit_Create函数不存在"
        End If
	Else
    	Print "DLL不存在" 
	End If
	Return m_hEle
End Function

'XC_API HELE WINAPI XEdit_CreateEx(int x, int y, int cx, int cy, edit_type_ type, HXCGUI hParent = NULL);
Function clsXEdit.CreateEx(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal nType As edit_type_,ByVal hParent As HXCGUI = NULL) As HELE
	If pLib Then   
        Dim pFunc As Function(ByVal As Long,ByVal As Long,ByVal As Long,ByVal As Long,ByVal As edit_type_,ByVal As HXCGUI) As HELE
        pFunc = DyLibSymbol(pLib,"XEdit_CreateEx") 
        If pFunc Then 
            m_hEle = pFunc(x,y,cx,cy,nType,hParent)
        End If
	End If
	Return m_hEle
End Function

'XC_API void WINAPI XEdit_EnableAutoWrap(HELE hEle, BOOL bEnable);
Sub clsXEdit.EnableAutoWrap(ByVal bEnable As BOOL)
	If pLib Then   
        Dim pFunc As Sub(ByVal As HELE,ByVal As BOOL)
        pFunc = DyLibSymbol(pLib,"XEdit_EnableAutoWrap") 
        If pFunc Then 
            pFunc(m_hEle,bEnable)
        Else
    		Print "XEdit_EnableAutoWrap函数不存在"
        End If
	Else
    	Print "DLL不存在" 
    End If    
End Sub

'XC_API void WINAPI XEdit_SetText(HELE hEle, const wchar_t* pString);
Sub clsXEdit.SetText(ByVal pString As LPCWSTR)
	If pLib Then   
        Dim pFunc As Sub(ByVal As HELE,ByVal As LPCWSTR)
        pFunc = DyLibSymbol(pLib,"XEdit_SetText") 
        If pFunc Then 
            pFunc(m_hEle,pString)
        Else
    		Print "XEdit_SetText函数不存在"
        End If
	Else
    	Print "DLL不存在"  
    End If   
End Sub

'XC_API int  WINAPI XEdit_AddStyle(HELE hEle, HXCGUI hFont_image_Obj, COLORREF color, BOOL bColor);
Function clsXEdit.AddStyle(ByVal hFont_image_Obj As HXCGUI,ByVal nColor As COLORREF,ByVal bColor As BOOL) As Long
	If pLib Then   
        Dim pFunc As Function(ByVal As HELE,ByVal As HXCGUI,ByVal As COLORREF,ByVal As BOOL) As Long  
        pFunc = DyLibSymbol(pLib,"XEdit_AddStyle") 
        If pFunc Then 
            Return pFunc(m_hEle,hFont_image_Obj,nColor,bColor)
        Else
    		Print "XEdit_AddStyle函数不存在"
        End If
	Else
    	Print "DLL不存在"  
	End If   
End Function

