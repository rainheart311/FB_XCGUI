#ifndef __XCGUI_BI__
#define __XCGUI_BI__

'/////////////////////////////////////////////////////////////////////
'/////////////////////句柄类型定义//////////////////////////////////////
'/////////////////////////////////////////////////////////////////////
'/// @defgroup groupHandle   句柄类型
'{ //@别名  炫彩句柄
Type HXCGUI As Integer		'///<资源句柄
'//@别名  窗口句柄
Type HWINDOW As Integer		'///<窗口资源句柄    
'//@别名  元素句柄
Type HELE As Integer		'///<元素资源句柄
'//@别名  菜单句柄
Type HMENUX As Integer		'///<菜单资源句柄
'//@别名  绘图句柄
Type HDRAW As Integer		'///<图形绘制资源句柄
'//@别名  图片句柄
Type HIMAGE As Integer		'///<图片资源句柄
'//@别名  字体句柄
Type HFONTX As Integer		'///<炫彩字体句柄
'//@别名  背景句柄
Type HBKM As Integer		'///<背景内容管理器句柄
'//@别名  模板句柄
Type HTEMP As Integer		'///<模板句柄
'//@别名  SVG句柄
Type HSVG As Integer		'///<SVG句柄
'}

'{ 颜色操作宏定义
#ifndef  GetAValue
#define  GetAValue(RGBA)         		(LoByte((RGBA) Shr 24))
#endif

#ifndef  RGBA
#define  RGBA(r, g, b, a)        		(Cast(UByte,a) Shl 24 Or Cast(UByte,r) Or Cast(UByte,g) Shl 8 Or Cast(UByte,b) Shl 16)
#endif

#define  COLOR_MAKE(r,g,b,a)     		(Cast(UByte,a) Shl 24 Or Cast(UByte,r) Or Cast(UByte,g) Shl 8 Or Cast(UByte,b) Shl 16)
#define  COLORREF_MAKE(r,g,b,a)  		(Cast(UByte,a) Shl 24 Or Cast(UByte,r) Or Cast(UByte,g) Shl 8 Or Cast(UByte,b) Shl 16)

#define  COLORREF_MAKE2(RGB,a)			(Cast(UByte,a) Shl 24 Or GetRValue(RGB) Or GetGValue(RGB) Shl 8 Or GetBValue(RGB) Shl 16)
#define  COLORREF_SET_RGB(nColor,nRGB)	((nColor & &HFF000000) Or (nRGB & &HFFFFFF))
#define  COLORREF_SET_A(nColor,a)		((nColor & &HFFFFFF) Or (Cast(UByte,a) Shl 24))
#define  COLORREF_GET_A(nColor)			(Cast(UByte,nColor Shr 24))
'}

'=============================================================================================
'{ 炫彩对象类型及样式
'//对象类型决定功能
'//对象样式决定外观
'/// @defgroup group_type_style_  对象类型及样式
'/// @{
'
'/// @defgroup group_ObjectType 对象句柄类型(XC_OBJECT_TYPE)
'/// @{
Enum XC_OBJECT_TYPE
    XC_ERROR                =-1,		'///<错误类型
    XC_NOTHING              =0,    		'///<啥也不是
    XC_WINDOW               =1,    		'///<窗口
    XC_MODALWINDOW          =2,    		'///<模态窗口
    XC_FRAMEWND             =3,   		'///<框架窗口
    XC_FLOATWND             =4,    		'///<浮动窗口
    XC_COMBOBOXWINDOW       =11,   		'///<组合框弹出下拉列表窗口 comboBoxWindow_         
    XC_POPUPMENUWINDOW      =12 ,  		'///<弹出菜单主窗口 popupMenuWindow_       
    XC_POPUPMENUCHILDWINDOW =13,   		'///<弹出菜单子窗口 popupMenuChildWindow_  
    XC_OBJECT_UI      		=19,   		'///<可视对象
    XC_WIDGET_UI     		=20,   		'///<窗口组件
	'//元素
    XC_ELE            		=21,   		'///<基础元素
    XC_ELE_LAYOUT     		=53,   		'///<布局元素
    XC_LAYOUT_FRAME   		=54,   		'///<流式布局
    XC_BUTTON         		=22,   		'///<按钮
    XC_EDIT           		=45,   		'///<编辑框
    XC_EDITOR         		=46,   		'///<代码编辑框

    XC_RICHEDIT       		=23,   		'///<富文本编辑框(已废弃), 请使用XC_EDIT
    XC_COMBOBOX       		=24,   		'///<下拉组合框
    XC_SCROLLBAR      		=25,   		'///<滚动条
    XC_SCROLLVIEW     		=26,   		'///<滚动视图
    XC_LIST           		=27,   		'///<列表
    XC_LISTBOX        		=28,   		'///<列表框
    XC_LISTVIEW       		=29,   		'///<列表视图,大图标
    XC_TREE           		=30,   		'///<列表树
    XC_MENUBAR        		=31,  		'///<菜单条
    XC_SLIDERBAR      		=32,   		'///<滑动条
    XC_PROGRESSBAR    		=33,   		'///<进度条
    XC_TOOLBAR        		=34,   		'///<工具条
    XC_MONTHCAL       		=35,   		'///<月历卡片
    XC_DATETIME       		=36,   		'///<日期时间
    XC_PROPERTYGRID   		=37,   		'///<属性网格
    XC_EDIT_COLOR     		=38,   		'///<颜色选择框
    XC_EDIT_SET       		=39,   		'///<设置编辑框
    XC_TABBAR         		=40,   		'///<tab条
    XC_TEXTLINK       		=41,   		'///<文本链接按钮

    XC_PANE                 =42,   		'///<窗格
    XC_PANE_SPLIT           =43,   		'///<窗格拖动分割条
    XC_MENUBAR_BUTTON       =44,   		'///<菜单条上的按钮
    'XC_TOOLBAR_BUTTON       =45,   	'///<工具条上按钮
    'XC_PROPERTYPAGE_LABEL   =46,   	'///<属性页标签按钮
    'XC_PIER                 =47,   	'///<窗格停靠码头
    'XC_BUTTON_MENU          =48,   	'///<弹出菜单按钮
    'XC_VIRTUAL_ELE          =49,   	'///<虚拟元素
    XC_EDIT_FILE            =50,   		'///<EditFile 文件选择编辑框
    XC_EDIT_FOLDER          =51,   		'///<EditFolder  文件夹选择编辑框
    XC_LIST_HEADER          =52,   		'///<列表头元素

    XC_SHAPE             	=61,   		'///<形状对象
    XC_SHAPE_TEXT        	=62,    	'///<形状对象-文本
    XC_SHAPE_PICTURE     	=63,    	'///<形状对象-图片
    XC_SHAPE_RECT        	=64,    	'///<形状对象-矩形
    XC_SHAPE_ELLIPSE     	=65,    	'///<形状对象-圆
    XC_SHAPE_LINE        	=66,    	'///<形状对象-直线
    XC_SHAPE_GROUPBOX    	=67,    	'///<形状对象-组框
    XC_SHAPE_GIF         	=68,    	'///<形状对象-GIF
    XC_SHAPE_TABLE       	=69,    	'///<形状对象-表格

	'//其他类型
    XC_MENU              	=81,   		'///<弹出菜单
    XC_IMAGE             	=82,        '///<图片
    XC_IMAGE_TEXTURE     	=XC_IMAGE,  '///<图片纹理,图片源,图片素材
    XC_HDRAW             	=83,   		'///<绘图操作
    XC_FONT              	=84,   		'///<炫彩字体
    'XC_FLASH             	=85,   		'///<flash  
    'XC_PANE_CELL         	=86,   		'///<...
    'XC_WEB               	=87,   		'///<...
    XC_IMAGE_FRAME       	=88,   		'///<图片帧,指定图片的渲染属性
    XC_SVG               	=89,   		'///<SVG矢量图形

    XC_LAYOUT_OBJECT      	=101, 		'///<布局对象LayoutObject, 已废弃
    XC_ADAPTER            	=102, 		'///<数据适配器Adapter
    XC_ADAPTER_TABLE      	=103, 		'///<数据适配器AdapterTable
    XC_ADAPTER_TREE       	=104, 		'///<数据适配器AdapterTree
    XC_ADAPTER_LISTVIEW   	=105, 		'///<数据适配器AdapterListView
    XC_ADAPTER_MAP        	=106, 		'///<数据适配器AdapterMap
    XC_BKINFOM            	=116, 		'///<背景管理器

	'//无实体对象,只是用来判断布局
    XC_LAYOUT_LISTVIEW     = 111,  		'///<内部使用
    XC_LAYOUT_LIST         = 112,  		'///<内部使用
    XC_LAYOUT_OBJECT_GROUP = 113,  		'///<内部使用
    XC_LAYOUT_OBJECT_ITEM  = 114,  		'///<内部使用
    XC_LAYOUT_PANEL        = 115,  		'///<内部使用

    '无实体对象,只是用来判断类型
    'XC_LIST_ITEM       =121,     		'//列表项模板 list_item
    'XC_LISTVIEW_GROUP  =122,			'
    'XC_LISTVIEW_ITEM   =123,			'
    XC_LAYOUT_BOX      =124,      		'///<布局盒子,复合类型

    XC_ANIMATION_SEQUENCE =131,   		'///<动画序列
    XC_ANIMATION_GROUP    =132,   		'///<动画同步组
    XC_ANIMATION_ITEM     =133    		'///<动画项
End Enum
'///@}

'/// @defgroup group_ObjectTypeEx   	对象扩展类型(XC_OBJECT_TYPE_EX,功能扩展)
'/// @{
Enum XC_OBJECT_TYPE_EX
    xc_ex_error = -1,        			'///<错误类型
    button_type_default = 0, 			'///<默认类型
    button_type_radio,       			'///<单选按钮
    button_type_check,       			'///<多选按钮
    button_type_close,       			'///<窗口关闭按钮
    button_type_min,         			'///<窗口最小化按钮
    button_type_max,         			'///<窗口最大化还原按钮

    element_type_layout      			'///<布局元素,启用布局功能的元素
End Enum
'///@}

'/// @defgroup group_ObjectStyle 对象样式(XC_OBJECT_STYLE,用于区分外观)
'/// @{
Enum  XC_OBJECT_STYLE
	xc_style_default     =0,
	button_style_default = xc_style_default,	'///<默认风格
	button_style_radio,                 		'///<单选按钮
	button_style_check,                 		'///<多选按钮
	button_style_icon,                  		'///<图标按钮		   
	button_style_expand,                		'///<展开按钮
	
	button_style_close,			        		'///<关闭按钮
	button_style_max,			        		'///<最大化按钮
	button_style_min,			        		'///<最小化按钮

	button_style_scrollbar_left,         		'///<水平滚动条-左按钮
	button_style_scrollbar_right,        		'///<水平滚动条-右按钮
	button_style_scrollbar_up,           		'///<垂直滚动条-上按钮
	button_style_scrollbar_down,         		'///<垂直滚动条-下按钮
	button_style_scrollbar_slider_h,     		'///<水平滚动条-滑块
	button_style_scrollbar_slider_v,     		'///<垂直滚动条-滑块

	button_style_tabBar,                 		'///<Tab条-按钮
	button_style_slider,                 		'///<滑动条-滑块

	button_style_toolBar,                		'///<工具条-按钮
	button_style_toolBar_left,           		'///<工具条-左滚动按钮
	button_style_toolBar_right,          		'///<工具条-右滚动按钮

	button_style_pane_close,             		'///<窗格-关闭按钮
	button_style_pane_lock,              		'///<窗格-锁定按钮
	button_style_pane_menu,              		'///<窗格-菜单按钮

	button_style_pane_dock_left,         		'///<窗格-码头按钮左
	button_style_pane_dock_top,          		'///<窗格-码头按钮上
	button_style_pane_dock_right,        		'///<窗格-码头按钮右
	button_style_pane_dock_bottom,       		'///<窗格-码头按钮下

	element_style_frameWnd_dock_left,    		'///<框架窗口-停靠码头左
	element_style_frameWnd_dock_top,     		'///<框架窗口-停靠码头上
	element_style_frameWnd_dock_right,   		'///<框架窗口-停靠码头右
	element_style_frameWnd_dock_bottom,  		'///<框架窗口-停靠码头下

	element_style_toolBar_separator,     		'///<工具条-分割线
	listBox_style_comboBox,              		'///<组合框-下拉列表框  ,下拉组合框弹出的ListBox
End Enum
'///@}

'/// @defgroup group_WindowStyle  窗口样式(window_style_)
'/// @{
Enum window_style_
	window_style_nothing         = &H0000,   	'///<什么也没有
	window_style_caption         = &H0001,   	'///<标题栏
	window_style_border          = &H0002,   	'///<边框,如果没有指定,那么边框大小为0
	window_style_center          = &H0004,   	'///<窗口居中
	window_style_drag_border     = &H0008,   	'///<拖动窗口边框
	window_style_drag_window     = &H0010,   	'///<拖动窗口
	window_style_allow_maxWindow = &H0020,   	'///<允许窗口最大化

	window_style_icon            = &H0040,   	'///<图标
	window_style_title           = &H0080,   	'///<标题
	window_style_btn_min         = &H0100,   	'///<控制按钮-最小化
	window_style_btn_max         = &H0200,   	'///<控制按钮-最大化
	window_style_btn_close       = &H0400,   	'///<控制按钮-关闭

	'///窗口样式-控制按钮: 居中 图标, 标题, 关闭按钮, 最大化按钮, 最小化按钮
	window_style_default=(window_style_caption or window_style_border or window_style_center or _
		window_style_drag_border or window_style_allow_maxWindow or window_style_icon or _
		window_style_title or window_style_btn_min or window_style_btn_max or window_style_btn_close),

	'///窗口样式-简单: 居中
	window_style_simple=(window_style_caption or window_style_border or window_style_center or window_style_drag_border Or window_style_allow_maxWindow),

	'///窗口样式-弹出窗口: 图标, 标题, 关闭按钮
	window_style_pop = (window_style_caption or window_style_border or window_style_center Or _
		window_style_drag_border or window_style_allow_maxWindow or window_style_icon Or _
		window_style_title Or window_style_btn_close),

	'///模态窗口样式-控制按钮: 居中, 图标, 标题, 关闭按钮
	window_style_modal=(window_style_caption or window_style_border or window_style_center Or _
		window_style_icon or window_style_title Or window_style_btn_close),
	
	'///模态窗口样式-简单: 居中
	window_style_modal_simple = (window_style_caption or window_style_border Or window_style_center)
End Enum
'///@}
'///@}
'}
'=============================================================================================
'{ 宏定义
'/// @defgroup group_macro_def  宏定义
#define  CLOUDUI_flag_openUrl                   1
#define  CLOUDUI_flag_downloadFile              2
#define  CLOUDUI_flag_downloadFileComplete      3
#define  CLOUDUI_flag_complete                  4

'/// @name  窗口位置
'/// @{
#define  WINDOW_TOP            1 				'//上
#define  WINDOW_BOTTOM         2 				'//下
#define  WINDOW_LEFT           3 				'//左
#define  WINDOW_RIGHT          4 				'//右
#define  WINDOW_TOPLEFT        5 				'//左上角
#define  WINDOW_TOPRIGHT       6 				'//右上角
#define  WINDOW_BOTTOMLEFT     7 				'//左下角
#define  WINDOW_BOTTOMRIGHT    8 				'//右下角
#define  WINDOW_CAPTION        9 				'//标题栏移动窗口区域
#define  WINDOW_BODY           10
'/// @}

'/// @name  特殊ID
'/// @{
#define  XC_ID_ROOT            0   				'///<根节点
#define  XC_ID_ERROR          -1   				'///<ID错误
#define  XC_ID_FIRST          -2   				'///<插入开始位置
#define  XC_ID_LAST           -3   				'///<插入末尾位置
'/// @}

'///@name 菜单ID , 当前未使用
'///@{
#define  IDM_CLIP          1000000000    		'///<剪切
#define  IDM_COPY          1000000001    		'///<复制
#define  IDM_PASTE         1000000002    		'///<粘贴
#define  IDM_DELETE        1000000003    		'///<删除
#define  IDM_SELECTALL     1000000004    		'///<全选
#define  IDM_DELETEALL     1000000005   		'///<清空
'///@}


'//窗格菜单 当前未使用
#define  IDM_LOCK          1000000006    		'///<锁定
#define  IDM_DOCK          1000000007    		'///<停靠
#define  IDM_FLOAT         1000000008    		'///<浮动
#define  IDM_HIDE          1000000009    		'///<隐藏

'//#define  edit_style_no        0  			'///<无效样式
#define  edit_style_default   1  				'///<edit 默认样式

'}
'=============================================================================================
'{ 枚举定义
'/// @defgroup  group_enum   枚举类型
'/// @{
'
'/// @defgroup groupWindow_position 窗口位置(window_position_)
'/// @{
Enum window_position_
	window_position_error=-1,  '///<错误
	window_position_top=0,     '///<top
	window_position_bottom,    '///<bottom
	window_position_left,      '///<left
	window_position_right,     '///<right
	window_position_body,      '///<body
	window_position_window,    '///<window 整个窗口 
End Enum
'///@}

'/// @defgroup groupElement_position UI元素位置(element_position_)
'/// @{
Enum element_position_
	element_position_no     = &H00,     '///<无效
	element_position_left   = &H01,     '///<左边
	element_position_top    = &H02,		'///<上边
	element_position_right  = &H04,		'///<右边
	element_position_bottom = &H08,		'///<下边
End Enum
'///@}

'/// @defgroup group_position 位置标识(element_position_)
'/// @{
Enum position_flag_
	position_flag_left,          '///<左
	position_flag_top,           '///<上
	position_flag_right,         '///<右
	position_flag_bottom,        '///<下
	position_flag_leftTop,       '///<左上角
	position_flag_leftBottom,    '///<左下角
	position_flag_rightTop,      '///<右上角
	position_flag_rightBottom,   '///<右下角
	position_flag_center,        '///<中心
End Enum
'///@}

'//透明窗口
'/// @defgroup groupWindowTransparent 炫彩窗口透明标识(window_transparent_)
'/// @{
Enum window_transparent_
	window_transparent_false=0,   '///<默认窗口,不透明
	window_transparent_shaped,    '///<透明窗口,带透明通道,异型
	window_transparent_shadow,    '///<阴影窗口,带透明通道,边框阴影,窗口透明或半透明
	window_transparent_simple,    '///<透明窗口,不带透明通道,指定半透明度,指定透明色
	window_transparent_win7,      '///<WIN7玻璃窗口,需要WIN7开启特效,当前未启用,当前未启用.
End Enum
'///@}


'/// @defgroup groupMenu 弹出菜单(menu)
'/// @{
'
'///@name 弹出菜单项标识(menu_item_flag_)
'///@{
Enum menu_item_flag_
	menu_item_flag_normal=   &H00,   '///<正常
	menu_item_flag_select=   &H01,   '///<选择或鼠标停留
	menu_item_flag_stay =    &H01,   '///<选择或鼠标停留 等于 menu_item_flag_select
	menu_item_flag_check=    &H02,   '///<勾选
	menu_item_flag_popup=    &H04,   '///<弹出
	menu_item_flag_separator=&H08,   '///<分隔栏 ID号任意,ID号被忽略
	menu_item_flag_disable=  &H10,   '///<禁用
End Enum
'///@}

'///@name 弹出菜单方向(menu_popup_position_)
'///@{
Enum menu_popup_position_
	menu_popup_position_left_top=0,      '///<左上角
	menu_popup_position_left_bottom,     '///<左下角
	menu_popup_position_right_top,       '///<右上角
	menu_popup_position_right_bottom,    '///<右下角
	menu_popup_position_center_left,     '///<左居中
	menu_popup_position_center_top,      '///<上居中
	menu_popup_position_center_right,    '///<右居中
	menu_popup_position_center_bottom,   '///<下居中
End Enum
'///@}
'///@}

'/// @defgroup groupImageDrawType 图片绘制类型(image_draw_type_)
'/// @{
Enum image_draw_type_
	image_draw_type_default=0,     		'///<默认
	image_draw_type_stretch,       		'///<拉伸
	image_draw_type_adaptive,     		'///<自适应,九宫格
	image_draw_type_tile,          		'///<平铺
	image_draw_type_fixed_ratio,   		'///<固定比例,当图片超出显示范围时,按照原始比例压缩显示图片
	image_draw_type_adaptive_border,  	'///<九宫格不绘制中间区域
End Enum
'///@}


'//状态--------------------------------------
'/// @defgroup groupCommonState3 普通三种状态(common_state3_)
'/// @{
Enum common_state3_
	common_state3_leave=0,  '///<离开
	common_state3_stay,     '///<停留
	common_state3_down,     '///<按下
End Enum
'///@}

'/// @defgroup groupButtonStateFlag 按钮状态(button_state_)
'/// @{
Enum button_state_
	button_state_leave=0,   '///<离开状态
	button_state_stay,      '///<停留状态
	button_state_down,      '///<按下状态
	button_state_check,     '///<选中状态
	button_state_disable,   '///<禁用状态
End Enum
'///@}

'/// @defgroup   groupComboBoxState 组合框状态(comboBox_state_)
'/// @{
Enum comboBox_state_
	comboBox_state_leave=0,   '///<鼠标离开状态
	comboBox_state_stay=1,    '///<鼠标停留状态
	comboBox_state_down=2,    '///<按下状态
End Enum
'///@}

'/// @defgroup groupListItemState 列表项状态(list_item_state_)
'/// 适用于(列表,列表框,列表视图)
'/// @{
Enum list_item_state_
	list_item_state_leave=0,   '///<项鼠标离开状态
	list_item_state_stay=1,    '///<项鼠标停留状态
	list_item_state_select=2,  '///<项选择状态
	list_item_state_cache=3,   '///<缓存的项
End Enum
'///@}

'/// @defgroup groupTreeItemState  列表树项状态(tree_item_state_)
'/// @{
Enum tree_item_state_
	tree_item_state_leave=0,   '///<项鼠标离开状态
	tree_item_state_stay=1,    '///<项鼠标停留状态
	tree_item_state_select=2,  '///<项选择状态
End Enum
'///@}

'//按钮图标对齐方式
'/// @defgroup groupButtonIconAlign 按钮图标对齐方式(button_icon_align_)
'/// @{
Enum button_icon_align_
	button_icon_align_left=0,  '///<图标在左边
	button_icon_align_top,     '///<图标在顶部
	button_icon_align_right,   '///<图标在右边
	button_icon_align_bottom,  '///<图标在底部
End Enum
'///@}

'/// @defgroup  groupListDrawItemBkFlag  项背景绘制标志位(List,ListBox,ListView,Tree)
'/// @{
Enum list_drawItemBk_flag_
	list_drawItemBk_flag_nothing = &H000,     '///<不绘制
	list_drawItemBk_flag_leave = &H001,       '///<绘制鼠标离开状态项背景
	list_drawItemBk_flag_stay = &H002,        '///<绘制鼠标选择状态项背景
	list_drawItemBk_flag_select = &H004,      '///<绘制鼠标停留状态项项背景
	list_drawItemBk_flag_group_leave = &H008, '///<绘制鼠标离开状态组背景,当项为组时
	list_drawItemBk_flag_group_stay = &H010,  '///<绘制鼠标停留状态组背景,当项为组时

	list_drawItemBk_flag_line  =&H020,        '///<列表绘制水平分割线
	list_drawItemBk_flag_lineV =&H040,        '///<列表绘制垂直分割线
End Enum
'/// @}

'//弹出消息框类型
'/// @defgroup groupMessageBox 弹出消息框(messageBox_flag_)
'/// @{
Enum messageBox_flag_
	messageBox_flag_other= &H00,    '///<其他
	messageBox_flag_ok=    &H01,    '///<确定按钮
	messageBox_flag_cancel=&H02,    '///<取消按钮
	
	messageBox_flag_icon_appicon =&H01000,  '///<图标 应用程序  IDI_APPLICATION
	messageBox_flag_icon_info    =&H02000,  '///<图标 信息     IDI_ASTERISK
	messageBox_flag_icon_qustion =&H04000,  '///<图标 问询/帮助/提问   IDI_QUESTION
	messageBox_flag_icon_error   =&H08000,  '///<图标 错误/拒绝/禁止  IDI_ERROR
	messageBox_flag_icon_warning =&H10000,  '///<图标 警告       IDI_WARNING
	messageBox_flag_icon_shield  =&H20000,  '///<图标 盾牌/安全   IDI_SHIELD
End Enum
'///@}

'///@defgroup GroupPropertyGrid_item_type    属性网格项类型(propertyGrid_item_type_)
'///@{
Enum propertyGrid_item_type_
	propertyGrid_item_type_text=0,      '///<默认,字符串类型
	propertyGrid_item_type_edit,        '///<编辑框
	propertyGrid_item_type_edit_color,  '///<颜色选择元素
	propertyGrid_item_type_edit_file,   '///<文件选择编辑框
	propertyGrid_item_type_edit_set,    '///<设置
	propertyGrid_item_type_comboBox,    '///<组合框
	propertyGrid_item_type_group,       '///<组
	propertyGrid_item_type_panel,       '///<面板
End Enum
'///@}

'///@defgroup  GroupZOrder    Z序位置(zorder_)
'///@{
Enum zorder_
	zorder_top,    '///<最上面
	zorder_bottom, '///<最下面
	zorder_before, '///<指定目标下面
	zorder_after,  '///<指定目标上面
End Enum
'///@}

'///@defgroup Group_pane_align_  窗格对齐(pane_align_)
'///@{
Enum pane_align_
	pane_align_error=-1,  '///<错误
	pane_align_left=0,    '///<左侧
	pane_align_top,       '///<顶部
	pane_align_right,     '///<右侧
	pane_align_bottom,    '///<底部
	pane_align_center,    '///<居中
End Enum
'///@}

'///@defgroup Group_layout_align_  布局对齐(layout_align_)
'///@{
Enum layout_align_
	layout_align_left = 0,		  '///<左侧
	layout_align_top,			  '///<顶部
	layout_align_right,			  '///<右侧
	layout_align_bottom,		  '///<底部
	layout_align_center,		  '///<居中
	layout_align_equidistant,     '///<等距
End Enum
'///@}

'/// @defgroup groupLayoutSize 布局大小类型(layout_size_)
'/// @{
Enum layout_size_
	layout_size_fixed = 0, '///<固定大小
	layout_size_fill,      '///<fill 填充父
	layout_size_auto,      '///<auto 自动大小,根据内容计算大小
	layout_size_weight,    '///<weight 比例,按照比例分配剩余空间
	layout_size_percent,   '///<百分比
	layout_size_disable,   '///<disable 不使用
End Enum
'///@}

'/// @defgroup  groupLayoutAlignAxis 布局轴对齐(layout_align_axis_)
'/// @{
Enum layout_align_axis_
	layout_align_axis_auto = 0,   '///<无
	layout_align_axis_start,      '///<水平布局(顶部), 垂直布局(左侧)
	layout_align_axis_center,     '///<居中
	layout_align_axis_end,        '///<水平布局(底部), 垂直布局(右侧)
End Enum
'///@}

'///@defgroup Group_edit_textAlign_flag_  编辑框文本对齐(edit_textAlign_flag_)
'///@{
Enum edit_textAlign_flag_
	edit_textAlign_flag_left    = &H0,   '///<左侧
	edit_textAlign_flag_right   = &H1,   '///<右侧
	edit_textAlign_flag_center  = &H2,   '///<水平居中

	edit_textAlign_flag_top      = &H0,  '///<顶部
	edit_textAlign_flag_bottom   = &H4,  '///<底部
	edit_textAlign_flag_center_v = &H8,  '///<垂直居中
End Enum
'///@}

'///@defgroup Group_pane_state_  窗格状态(pane_state_)
'///@{
Enum pane_state_
	pane_state_error=-1,
	pane_state_any=0,
	pane_state_lock,   '///<锁定
	pane_state_dock,   '///<停靠码头
	pane_state_float,  '///<浮动窗格
End Enum
'///@}

'///@defgroup Group_textFormatFlag_    文本对齐(textFormatFlag_)
'///@{
Enum textFormatFlag_
	textAlignFlag_left     =0,      '///<左对齐
	textAlignFlag_top      =0,      '///<垂直顶对齐
	textAlignFlag_left_top =&H4000, '///<内部保留
	textAlignFlag_center   =&H1,    '///<水平居中
	textAlignFlag_right    =&H2,    '///<右对齐.

	textAlignFlag_vcenter  =&H4,    '///<垂直居中
	textAlignFlag_bottom   =&H8,    '///<垂直底对齐

	textFormatFlag_DirectionRightToLeft=&H10,   '///<从右向左顺序显示文本
	textFormatFlag_NoWrap              =&H20,   '///<禁止换行
	textFormatFlag_DirectionVertical   =&H40,   '///<垂直显示文本
	textFormatFlag_NoFitBlackBox       =&H80,   '///<允许部分字符延伸该字符串的布局矩形。默认情况下，将重新定位字符以避免任何延伸
	textFormatFlag_DisplayFormatControl=&H100,  '///<控制字符（如从左到右标记）随具有代表性的标志符号一起显示在输出中。
	textFormatFlag_NoFontFallback  =&H200,      '///<对于请求的字体中不支持的字符，禁用回退到可选字体。缺失的任何字符都用缺失标志符号的字体显示，通常是一个空的方块
	textFormatFlag_MeasureTrailingSpaces=&H400, '///<包括每一行结尾处的尾随空格。在默认情况下，MeasureString 方法返回的边框都将排除每一行结尾处的空格。设置此标记以便在测定时将空格包括进去
	textFormatFlag_LineLimit       =&H800,      '///<如果内容显示高度不够一行,那么不显示
	textFormatFlag_NoClip          =&H1000,     '///<允许显示标志符号的伸出部分和延伸到边框外的未换行文本。在默认情况下，延伸到边框外侧的所有文本和标志符号部分都被剪裁

	textTrimming_None              = 0,        	'///<不使用去尾
	textTrimming_Character         = &H40000,  	'///<以字符为单位去尾
	textTrimming_Word              = &H80000,  	'///<以单词为单位去尾
	textTrimming_EllipsisCharacter = &H8000,   	'///<以字符为单位去尾,省略部分使用且略号表示
	textTrimming_EllipsisWord      = &H10000,  	'///<以单词为单位去尾,
	textTrimming_EllipsisPath      = &H20000,  	'///<略去字符串中间部分，保证字符的首尾都能够显示  
End Enum
'///@}

'///@defgroup Group_textFormatFlag_dwrite_    D2D文本渲染模式(XC_DWRITE_RENDERING_MODE)
'///@{
Enum XC_DWRITE_RENDERING_MODE
	XC_DWRITE_RENDERING_MODE_DEFAULT = 0,                    '///<指定根据字体和大小自动确定呈现模式。
	XC_DWRITE_RENDERING_MODE_ALIASED,						 '///<指定不执行抗锯齿。 每个像素要么设置为文本的前景色，要么保留背景的颜色。
	XC_DWRITE_RENDERING_MODE_CLEARTYPE_GDI_CLASSIC,			 '///<使用与别名文本相同的度量指定 ClearType 呈现。 字形只能定位在整个像素的边界上。
	XC_DWRITE_RENDERING_MODE_CLEARTYPE_GDI_NATURAL,			 '///<使用使用 CLEARTYPE_NATURAL_QUALITY 创建的字体，使用与使用 GDI 的文本呈现相同的指标指定 ClearType 呈现。 与使用别名文本相比，字形度量更接近其理想值，但字形仍然位于整个像素的边界上。
	XC_DWRITE_RENDERING_MODE_CLEARTYPE_NATURAL,				 '///<仅在水平维度中指定具有抗锯齿功能的 ClearType 渲染。这通常用于中小字体大小（最多 16 ppem）。
	XC_DWRITE_RENDERING_MODE_CLEARTYPE_NATURAL_SYMMETRIC,	 '///<指定在水平和垂直维度上具有抗锯齿的 ClearType 渲染。这通常用于较大的尺寸，以使曲线和对角线看起来更平滑，但会牺牲一些柔和度。
	XC_DWRITE_RENDERING_MODE_OUTLINE,                        '///<指定渲染应绕过光栅化器并直接使用轮廓。 这通常用于非常大的尺寸。
End Enum
'///@}

'///@defgroup Group_listItemTemp_type_    列表项模板类型(listItemTemp_type_)
'///@{
Enum listItemTemp_type_
	listItemTemp_type_tree = &H01,					  													'///<tree
	listItemTemp_type_listBox = &H02,				  													'///<listBox
	listItemTemp_type_list_head = &H04,				 													'///<list 列表头
	listItemTemp_type_list_item = &H08,				  													'///<list 列表项
	listItemTemp_type_listView_group = &H10,		  													'///<listView 列表视组
	listItemTemp_type_listView_item = &H20,			  													'///<listView 列表视项
	listItemTemp_type_list = listItemTemp_type_list_head Or listItemTemp_type_list_item,  				'///<list (列表头)与(列表项)组合
	listItemTemp_type_listView = listItemTemp_type_listView_group Or listItemTemp_type_listView_item,	'///<listView (列表视组)与(列表视项)组合
End Enum
'///@}

'///@defgroup Group_adjustLayout    调整布局标识位(adjustLayout_)
'///@{
Enum adjustLayout_
	adjustLayout_no   = &H00,  			'///<不调整布局
	adjustLayout_all  = &H01,  			'///<强制调整自身和子对象布局.
	adjustLayout_self = &H02,  			'///<只调整自身布局,不调整子对象布局.
	'//xc_adjustLayout_free = &H03   调整布局,非强制性, 只调整坐标改变的对象
End Enum
'///@}

'/// @defgroup group_edit_macro 编辑框类型(edit_type_)
'/// @{
Enum edit_type_
	edit_type_none = 0,   '///<普通编辑框,   每行的高度相同
	edit_type_editor,     '///<代码编辑
	edit_type_richedit,   '///<富文本编辑框, 每行的高度可能不同
	edit_type_chat,       '///<聊天气泡, 每行的高度可能不同
	edit_type_codeTable,  '///<代码表格,内部使用,  每行的高度相同
End Enum

Enum edit_style_type_
	edit_style_type_font_color = 1,  '///<字体
	edit_style_type_image,           '///<图片
	edit_style_type_obj,             '///<UI对象
End Enum


'///Edit 聊天气泡对齐方式
Enum chat_flag_
	chat_flag_left = &H1,    			'///<左侧
	chat_flag_right = &H2,   			'///<右侧
	chat_flag_center = &H4,  			'///<中间
	chat_flag_next_row_bubble = &H8,	'///<下一行显示气泡
End Enum
'///@}

'/// @defgroup group_table  形状表格标识(table_flag_)
'/// @{

'///@name  形状表格标识(table_flag_)
'///@{
Enum table_flag_
	table_flag_full = 0,   '///<铺满组合单元格
	table_flag_none,       '///<正常最小单元格
End Enum
'///@}

'///@name  形状表格线标识(table_line_flag_)
'///@{
Enum table_line_flag_
	table_line_flag_left    = &H1,   '///<待补充
	table_line_flag_top     = &H2,   '///<待补充
	table_line_flag_right   = &H4,	 '///<待补充
	table_line_flag_bottom  = &H8,   '///<待补充
	table_line_flag_left2   = &H10,  '///<待补充
	table_line_flag_top2    = &H20,	 '///<待补充
	table_line_flag_right2  = &H40,	 '///<待补充
	table_line_flag_bottom2 = &H80,	 '///<待补充
End Enum
'///@}
'///@}

'/// @defgroup group_monthCal_button_type_    月历元素上的按钮类型(monthCal_button_type_)
'/// @{
Enum monthCal_button_type_
	monthCal_button_type_today = 0,  '///< 今天按钮
	monthCal_button_type_last_year,  '///< 上一年
	monthCal_button_type_next_year,  '///< 下一年
	monthCal_button_type_last_month, '///< 上一月
	monthCal_button_type_next_month, '///< 下一月
End Enum
'///@}

'///@defgroup  group_fontStyle_  字体样式(fontStyle_)
'///@{
Enum fontStyle_
	fontStyle_regular = 0,     '///<正常
	fontStyle_bold = 1,        '///<粗体
	fontStyle_italic = 2,      '///<斜体
	fontStyle_boldItalic = 3,  '///<粗斜体
	fontStyle_underline = 4,   '///<下划线
	fontStyle_strikeout = 8    '///<删除线
End Enum
'///@}

'///@defgroup  group_adapter_date_type_    数据适配器数据类型(adapter_date_type_)
'///@{
Enum adapter_date_type_
	adapter_date_type_error = -1,
	adapter_date_type_int = 0,     '///<整形
	adapter_date_type_float = 1,   '///<浮点型
	adapter_date_type_string = 2,  '///<字符串
	adapter_date_type_image = 3,   '///<图片
End Enum
'///@}

'/// @defgroup group_ease_type_  缓动类型(ease_type_)
'/// @{
Enum ease_type_
	easeIn,      '///<从慢到快
	easeOut,     '///<从快到慢
	easeInOut,   '///<从慢到快再到慢
End Enum
'///@}

'///@defgroup   group_ease_flag_  缓动标识(ease_flag_)
'///@{
Enum ease_flag_
	ease_flag_linear,			'///<线性, 直线
	ease_flag_quad,			    '///<二次方曲线
	ease_flag_cubic,			'///<三次方曲线, 圆弧
	ease_flag_quart,			'///<四次方曲线
	ease_flag_quint,			'///<五次方曲线

	ease_flag_sine,				'///<正弦, 在末端变化
	ease_flag_expo,			    '///<突击, 突然一下
	ease_flag_circ,		        '///<圆环, 好比绕过一个圆环
	ease_flag_elastic,		    '///<强力回弹
	ease_flag_back,				'///<回弹, 比较缓慢
	ease_flag_bounce,		    '///<弹跳, 模拟小球落地弹跳

	ease_flag_in    = &H010000, '///<从慢到快
	ease_flag_out   = &H020000, '///<从快到慢
	ease_flag_inOut = &H030000, '///<从慢到快再到慢
End Enum
'///@}

'///@defgroup   group_notifyMsg_skin_  通知消息外观(notifyMsg_skin_)
'///@{
Enum notifyMsg_skin_
	notifyMsg_skin_no,         '///<默认
	notifyMsg_skin_success,    '///<成功
	notifyMsg_skin_warning,	   '///<警告
	notifyMsg_skin_message,	   '///<消息
	notifyMsg_skin_error,	   '///<错误
End Enum
'///@}

'///@defgroup group_animation_move_  动画移动标识(animation_move_)
'///@{
Enum animation_move_
	animation_move_x = &H01,   '///<X轴移动
	animation_move_y = &H02,   '///<Y轴移动
End Enum
'///@}

'///@defgroup group_bkInfo_align_flag_  背景对象对齐方式(bkObject_align_flag_)
'///@{
Enum bkObject_align_flag_  '//背景对象对齐方式
	bkObject_align_flag_no     = &H000,    '///<无
	bkObject_align_flag_left   = &H001,    '///<左对齐, 当设置此标识时,外间距(margin.left)代表左侧间距; 当right未设置时,那么外间距(margin.right)代表宽度;
	bkObject_align_flag_top    = &H002,    '///<顶对齐, 当设置此标识时,外间距(margin.top)代表顶部间距; 当bottom未设置时,那么外间距(margin.bottom)代表高度;
	bkObject_align_flag_right  = &H004,    '///<右对齐, 当设置此标识时,外间距(margin.right)代表右侧间距; 当left未设置时,那么外间距(margin.left)代表宽度;
	bkObject_align_flag_bottom = &H008,    '///<底对齐, 当设置此标识时,外间距(margin.bottom)代表底部间距; 当top未设置时,那么外间距(margin.top)代表高度;
	bkObject_align_flag_center = &H010,    '///<水平居中, 当设置此标识时,外间距(margin.left)代表宽度;
	bkObject_align_flag_center_v = &H020,  '///<垂直居中, 当设置此标识时,外间距(margin.top)代表高度; 
End Enum
'///@}

'///@defgroup group_frameWnd_cell_type_  框架窗口单元格类型(frameWnd_cell_type_)
Enum frameWnd_cell_type_
	frameWnd_cell_type_no          = 0,   '///<无
	frameWnd_cell_type_pane        = 1,   '///<窗格
	frameWnd_cell_type_group       = 2,   '///<窗格组
	frameWnd_cell_type_bodyView    = 3,   '///<主视图区
	frameWnd_cell_type_top_bottom  = 4,   '///<上下布局
	frameWnd_cell_type_left_right  = 5,   '///<左右布局
End Enum
'///@}
'}
'=============================================================================================
'{ 炫彩组合状态
'/// @defgroup group_combo_StateFlag  组合状态
'/// @{
'
'///@defgroup group_window_state_flag_  窗口状态(window_state_flag_)
'///@{
'enum  window_state_flag_
'{
'	window_state_flag_nothing       =&H0000,  ///<无
'	window_state_flag_leave         =&H0001,  ///<整个窗口
'	window_state_flag_body_leave    =&H0002,  ///<窗口-body
'	window_state_flag_top_leave     =&H0004,  ///<窗口-top
'	window_state_flag_bottom_leave  =&H0008,  ///<窗口-bottom
'	window_state_flag_left_leave    =&H0010,  ///<窗口-left
'	window_state_flag_right_leave   =&H0020,  ///<窗口-right
'
'	window_state_flag_layout_body   =&H20000000, ///<布局内容区
'};
'///@}
'
'///@defgroup  group_element_state_flag_ 元素状态(element_state_flag_)
'///@{
'enum  element_state_flag_
'{
'	element_state_flag_nothing   =window_state_flag_nothing,  ///<无
'	element_state_flag_enable    =&H0001,  ///<启用
'	element_state_flag_disable   =&H0002,  ///<禁用
'	element_state_flag_focus     =&H0004,  ///<焦点
'	element_state_flag_focus_no  =&H0008,  ///<无焦点
'	element_state_flag_focusEx   =&H40000000,  ///<该元素或该元素的子元素拥有焦点
'	element_state_flag_focusEx_no=&H80000000,  ///<无焦点Ex
'
'	layout_state_flag_layout_body  =window_state_flag_layout_body, ///<布局内容区
'
'	element_state_flag_leave     =&H0010,  ///<鼠标离开
'	element_state_flag_stay      =&H0020,  ///<为扩展模块保留
'	element_state_flag_down      =&H0040,  ///<为扩展模块保留
'};
'///@}
'
'///@defgroup  group_button_state_flag_ 按钮状态(button_state_flag_)
'///@{
'enum  button_state_flag_
'{
'	button_state_flag_leave     =element_state_flag_leave,  ///<鼠标离开
'	button_state_flag_stay      =element_state_flag_stay,   ///<鼠标停留
'	button_state_flag_down      =element_state_flag_down,   ///<鼠标按下
'
'	button_state_flag_check     =&H0080, ///<选中
'	button_state_flag_check_no  =&H0100, ///<未选中
'	
'	button_state_flag_WindowRestore  =&H0200, ///<窗口还原
'	button_state_flag_WindowMaximize =&H0400, ///<窗口最大化
'};
'///@}
'
'///@defgroup   group_comboBox_state_flag_  组合框状态(comboBox_state_flag_)
'///@{
'enum comboBox_state_flag_
'{
'	comboBox_state_flag_leave   =element_state_flag_leave, ///<鼠标离开
'	comboBox_state_flag_stay    =element_state_flag_stay,  ///<鼠标停留
'	comboBox_state_flag_down    =element_state_flag_down,  ///<鼠标按下
'};
'///@}
'
'///@defgroup   group_listBox_state_flag_  列表框状态(listBox_state_flag_)
'///@{
'enum listBox_state_flag_
'{
'	listBox_state_flag_item_leave     =&H0080, ///<项鼠标离开
'	listBox_state_flag_item_stay      =&H0100, ///<项鼠标停留
'
'	listBox_state_flag_item_select    =&H0200, ///<项选择
'	listBox_state_flag_item_select_no =&H0400, ///<项未选择
'};
'///@}
'
'///@defgroup   group_list_state_flag_  列表状态(list_state_flag_)
'///@{
'enum list_state_flag_
'{
'	list_state_flag_item_leave      =&H0080, ///<项鼠标离开
'	list_state_flag_item_stay       =&H0100, ///<项鼠标停留
'
'	list_state_flag_item_select     =&H0200, ///<项选择
'	list_state_flag_item_select_no  =&H0400, ///<项未选择
'};
'///@}
'
'///@defgroup   group_listHeader_state_flag_  列表头状态(listHeader_state_flag_)
'///@{
'enum listHeader_state_flag_
'{
'	listHeader_state_flag_item_leave  =&H0080, ///<项鼠标离开
'	listHeader_state_flag_item_stay   =&H0100, ///<项鼠标停留
'	listHeader_state_flag_item_down   =&H0200, ///<项鼠标按下
'};
'///@}
'
'///@defgroup   group_listView_state_flag_ 列表视图状态(listView_state_flag_)
'///@{
'enum listView_state_flag_
'{
'	listView_state_flag_item_leave       =&H0080,  ///<项鼠标离开
'	listView_state_flag_item_stay        =&H0100,  ///<项鼠标停留
'
'	listView_state_flag_item_select      =&H0200,  ///<项选择
'	listView_state_flag_item_select_no   =&H0400,  ///<项未选择
'
'	listView_state_flag_group_leave      =&H0800,  ///<组鼠标离开
'	listView_state_flag_group_stay       =&H1000,  ///<组鼠标停留
'
'	listView_state_flag_group_select     =&H2000,  ///<组选择
'	listView_state_flag_group_select_no  =&H4000,  ///<组未选择
'};
'///@}
'
'///@defgroup   group_tree_state_flag_ 列表树状态(tree_state_flag_)
'///@{
'enum tree_state_flag_
'{
'	tree_state_flag_item_leave      =&H0080,  ///<项鼠标离开
'	tree_state_flag_item_stay       =&H0100,  ///<项鼠标停留,保留值, 暂未使用
'
'	tree_state_flag_item_select     =&H0200,  ///<项选择
'	tree_state_flag_item_select_no  =&H0400,  ///<项未选择
'
'	tree_state_flag_group           =&H0800,  ///<项为组
'	tree_state_flag_group_no        =&H1000,  ///<项不为组
'};
'///@}
'
'///@defgroup   group_monthCal_state_flag_  月历卡片状态(monthCal_state_flag_)
'///@{
'enum monthCal_state_flag_     
'{
'	monthCal_state_flag_leave = element_state_flag_leave,  ///<离开状态
'
'	monthCal_state_flag_item_leave      = &H0080,     ///< 项-离开
'	monthCal_state_flag_item_stay       = &H0100,     ///< 项-停留
'	monthCal_state_flag_item_down       = &H0200,     ///< 项-按下
'
'	monthCal_state_flag_item_select     = &H0400,     ///< 项-选择
'	monthCal_state_flag_item_select_no  = &H0800,     ///< 项-未选择
'
'	monthCal_state_flag_item_today      = &H1000,     ///< 项-今天
'//	monthCal_state_flag_item_other      = &H2000,     ///< 项-上月及下月
'	monthCal_state_flag_item_last_month = &H2000,     ///< 项-上月
'	monthCal_state_flag_item_cur_month  = &H4000,     ///< 项-当月
'	monthCal_state_flag_item_next_month = &H8000,     ///< 项-下月
'};
'///@}
'
'///@defgroup   group_propertyGrid_state_flag_  属性网格状态(propertyGrid_state_flag_)
'///@{
'enum propertyGrid_state_flag_     
'{
'	propertyGrid_state_flag_item_leave     = &H0080,  	 ///<离开
'	propertyGrid_state_flag_item_stay      = &H0100,	 ///<停留
'
'	propertyGrid_state_flag_item_select    = &H0200,	 ///<选择
'	propertyGrid_state_flag_item_select_no = &H0400,	 ///<未选择
'
'	propertyGrid_state_flag_group_leave      = &H0800,	 ///<组离开
'	propertyGrid_state_flag_group_expand     = &H1000,	 ///<组展开
'	propertyGrid_state_flag_group_expand_no  = &H2000,	 ///<组未展开
'};
'///@}
'
'///@defgroup   group_pane_state_flag_  窗格状态(pane_state_flag_)
'///@{
'enum pane_state_flag_     
'{
'	pane_state_flag_leave     = element_state_flag_leave,  ///<离开
'	pane_state_flag_stay      = element_state_flag_stay,   ///<停留
'
'	pane_state_flag_caption   = &H0080,  ///<标题
'	pane_state_flag_body      = &H0100,  ///<内容区
'};
'///@}
'
'///@defgroup   group_layout_state_flag_  布局状态(layout_state_flag_)
'///@{
'enum layout_state_flag_
'{
'	layout_state_flag_nothing   =window_state_flag_nothing,  ///<无
'	layout_state_flag_full      = &H0001,   ///<完整背景
'	layout_state_flag_body      = &H0002,   ///<内容区域, 不包含边大小
'};
'///@}
'///@}
'}
'=============================================================================================
'{ 定义结构体
'///@defgroup  group_struct_  结构体定义
'///@{
'typedef   struct lua_State lua_State;

Type RECTF				'struct  RECTF'{
	nleft As Single		'	float  left;
	ntop As Single		'	float  top;
	nright As Single	'	float  right;
	nbottom As Single	'	float  bottom;
End Type				'};

'///边大小
Type borderSize_		'struct borderSize_'{  //4条边的大小
	leftSize As Long	'	int  leftSize;   ///<左边大小
	topSize As Long		'	int  topSize;    ///<上边大小
	rightSize As Long	'	int  rightSize;  ///<右边大小
	bottomSize As Long	'	int  bottomSize; ///<下边大小
End Type				'};

Type spaceSize_ As borderSize_			'typedef  borderSize_  spaceSize_, paddingSize_, marginSize_;
Type paddingSize_ As borderSize_
Type marginSize_ As borderSize_

'///位置点
Type position_			'struct  position_'{
	iRow As Long		'	int  iRow;    ///<行索引
	iColumn As Long		'	int  iColumn; ///<列索引
End Type				'};

'///ListBox 列表框项信息
Type listBox_item_				'struct listBox_item_'{
	index As Long				'	int     index;      ///<项索引
	nUserData As Integer		'	vint    nUserData;  ///<用户绑定数据
	nHeight As Long				'	int     nHeight;    ///<项默认高度
	nSelHeight As Long			'	int     nSelHeight; ///<项选中时高度
	nState As list_item_state_	'	list_item_state_  nState;  ///<状态
	rcItem As RECT				'	RECT    rcItem;     ///<项坐标
	hLayout As HXCGUI			'	HXCGUI  hLayout;    ///<布局对象
	hTemp As HTEMP				'	HTEMP   hTemp;      ///<列表项模板
End Type						'};

'///ListBox 列表框项信息2
Type listBox_item_info_'struct listBox_item_info_'{
	nUserData As Integer'	vint    nUserData;  ///<用户绑定数据
	nHeight As Long'	int     nHeight;    ///<项高度, -1使用默认高度
	nSelHeight As Long'	int     nSelHeight; ///<项选中时高度, -1使用默认高度
End Type'};

'///ListView 列表视项ID
Type listView_item_id_'struct  listView_item_id_'{
	iGroup As Long'	int  iGroup;   ///<组索引
	iItem As Long'	int  iItem;    ///<项索引
End Type'};

'///List 列表项信息
Type list_item_'struct list_item_'{
	index As Long'	int     index;             ///<项索引(行索引)
	iSubItem As Long'	int     iSubItem;          ///<子项索引(列索引)
	nUserData As Integer'	vint    nUserData;         ///<用户数据
	nState As list_item_state_'	list_item_state_  nState;  ///<状态
	rcItem As RECT'	RECT    rcItem;     ///<未使用
	hLayout As HXCGUI'	HXCGUI  hLayout;    ///<布局对象
	hTemp As HTEMP'	HTEMP   hTemp;      ///<列表项模板
End Type'};

'///List 列表头项信息
Type list_header_item_'struct list_header_item_'{
	index As Long'	int     index;           ///<项索引
	nUserData As Integer'	vint    nUserData;       ///<用户数据
	
	bSort As BOOL'	BOOL    bSort;           ///<是否支持排序
	nSortType As Long'	int     nSortType;       ///<排序方式,0无效,1升序,2降序
	iColumnAdapter As Long'	int     iColumnAdapter;  ///<对应数据适配器中的列索引
	
	nState As common_state3_'	common_state3_  nState;  ///<状态
	rcItem As RECT'	RECT      rcItem;        ///<坐标
	hLayout As HXCGUI'	HXCGUI    hLayout;       ///<布局对象
	hTemp As HTEMP'	HTEMP     hTemp;         ///<列表项模板
End Type'};

'///Tree 树项信息
Type tree_item_'struct tree_item_'{
	nID As Long'	int     nID;				 ///<项ID
	nDepth As Long'	int     nDepth;				 ///<深度
	nHeight As Long'	int     nHeight;			 ///<项高度
	nSelHeight As Long'	int     nSelHeight;			 ///<项选中状态高度
	nUserData As Integer'	vint    nUserData;			 ///<用户数据
	bExpand As BOOL'	BOOL    bExpand;			 ///<展开
	nState As tree_item_state_'	tree_item_state_  nState;	 ///<状态
	rcItem As RECT'	RECT    rcItem;              ///<坐标
	hLayout As HXCGUI'	HXCGUI  hLayout;             ///<布局对象
	hTemp As HTEMP'	HTEMP   hTemp;               ///<列表项模板
End Type'};
'
'///ListView 列表视项信息
Type listView_item_'struct listView_item_'{
	iGroup As Long'	int     iGroup;            ///<项所述组索引 -1没有组
	iItem As Long'	int     iItem;             ///<项在数组中位置索引,如果此致为-1,那么为组
	nUserData As Integer'	vint    nUserData;         ///<用户绑定数据
	nState As list_item_state_'	list_item_state_  nState;  ///<状态  
	rcItem As RECT'	RECT    rcItem;            ///<整个区域,包含边框
	hLayout As HXCGUI'	HXCGUI  hLayout;           ///<布局对象
	hTemp As HTEMP'	HTEMP   hTemp;             ///<列表项模板  
End Type'};
'
'/// @defgroup group_menu_macro Menu菜单
'/// @{
'///菜单-弹出窗口信息
Type menu_popupWnd_'struct  menu_popupWnd_'{
	hWindow As HWINDOW'	HWINDOW hWindow;    ///<窗口句柄
	nParentID As Long'	int     nParentID;  ///<父项ID
End Type'};

'///菜单背景自绘结构
Type menu_drawBackground_'struct menu_drawBackground_'{
	hMenu As HMENUX'	HMENUX  hMenu;      ///<菜单句柄
	hWindow As HWINDOW'	HWINDOW hWindow;    ///<当前弹出菜单项的窗口句柄
	nParentID As Long'	int     nParentID;  ///<父项ID
End Type'};
'
'///菜单项自绘结构
Type menu_drawItem_'struct  menu_drawItem_'{
	hMenu As HMENUX'	HMENUX     hMenu;       ///<菜单句柄
	hWindow As HWINDOW'	HWINDOW    hWindow;     ///<当前弹出菜单项的窗口句柄
	nID As Long'	int        nID;         ///<ID
	nState As Long'	int        nState;	    ///<状态 @ref menu_item_flag_
	rcItem As RECT'	RECT       rcItem;      ///<坐标
	hIcon As HIMAGE'	HIMAGE     hIcon;       ///<菜单项图标
	pText As Const wchar_t Ptr'	const wchar_t* pText;   ///<文本
End Type'};
'///@}

'///树UI元素拖动项
Type tree_drag_item_'struct tree_drag_item_'{
	nDragItem As Long'	int  nDragItem;  ///< 拖动项ID
	nDestItem As Long'	int  nDestItem;  ///< 目标项ID
	nType As Long'	int  nType;      ///< 停放相对目标位置,0:(上)停放到目标的上面, 1:(下)停放到目标的下面, 3:(中)停放到目标的的子项, 
End Type'};

'///字体信息
Type font_info_'struct  font_info_'{
	nSize As Long'	int   nSize;                 ///<字体大小,单位(pt,磅).
	nStyle As Long'	int   nStyle;                ///<字体样式 fontStyle_
	szname(LF_FACESIZE) As wchar_t'	wchar_t  name[LF_FACESIZE];  ///<字体名称
End Type'};

'///PGrid 属性网格项信息
Type propertyGrid_item_'struct propertyGrid_item_'{
	nType As propertyGrid_item_type_'	propertyGrid_item_type_ nType; ///<类型
	nID As Long'	int   nID;           ///<项ID
	nDepth As Long'	int   nDepth;        ///<深度
	nUserData As Integer'	vint  nUserData;     ///<用户数据
	nNameColWidth As Long'	int   nNameColWidth; ///<名称列宽度
	
	rcItem As RECT'	RECT  rcItem;   ///<坐标
	rcExpand As RECT'	RECT  rcExpand; ///<展开
	bExpand As BOOL'	BOOL  bExpand;  ///<是否展开
	bShow As BOOL'	BOOL  bShow;    ///<是否可见
End Type'};

'///Edit 样式信息
Type edit_style_info_'struct edit_style_info_'{
	nType As UShort'	USHORT   type;    ///<样式类型
	nRef As UShort'	USHORT   nRef;              ///<引用计数
	hFont_image_obj As HXCGUI'	HXCGUI   hFont_image_obj;   ///<字体,图片,UI对象
	nColor As COLORREF'	COLORREF color;             ///<颜色
	bColor As BOOL'	BOOL     bColor;            ///<是否使用颜色
End Type'};

'///Edit 数据复制-样式
Type edit_data_copy_style_'struct edit_data_copy_style_'{
	hFont_image_obj As HIMAGE'	HIMAGE   hFont_image_obj; ///<字体,图片,UI对象
	nColor As COLORREF'	COLORREF color;           ///<颜色
	bColor As BOOL'	BOOL     bColor;          ///<是否使用颜色
End Type'};

'///Edit 数据复制
Type edit_data_copy_'struct edit_data_copy_'{
	nCount As Long'	int  nCount;       ///<内容数量
	nStyleCount As Long'	int  nStyleCount;  ///<样式数量
	pStyle As edit_data_copy_style_ Ptr'	edit_data_copy_style_* pStyle; ///<样式数组
	pData As UINT Ptr'	UINT* pData;       ///<内容数组 高位2字节:样式索引, 低位2字节:值
End Type'};
'
'///Editor 颜色信息
Type editor_color_								'struct editor_color_'{
	bAlignLineArrow As BOOL						'	BOOL       bAlignLineArrow;            	///<对齐线 - 是否显示箭头
	clrMargin1 As COLORREF						'	COLORREF   clrMargin1;                 	///<侧边栏 - 背景色1, 显示断点
	clrMargin2 As COLORREF						'	COLORREF   clrMargin2;                 	///<侧边栏 - 背景色2, 显示行号
	clrMargin_text As COLORREF					'	COLORREF   clrMargin_text;             	///<侧边栏 - 文本色 - 行号颜色
	clrMargin_breakpoint As COLORREF			'	COLORREF   clrMargin_breakpoint;       	///<侧边栏 - 断点色
	clrMargin_breakpointBorder As COLORREF		'	COLORREF   clrMargin_breakpointBorder; 	///<侧边栏 - 断点描边色
	clrMargin_runRowArrow As COLORREF			'	COLORREF   clrMargin_runRowArrow;      	///<侧边栏 - 调试位置箭头
	clrMargin_curRow As COLORREF				'	COLORREF   clrMargin_curRow;           	///<侧边栏 - 当前行指示色,光标所在行
	clrMargin_error As COLORREF					'	COLORREF   clrMargin_error;            	///<侧边栏 - 错误指示色
	
	clrCurRowFull As COLORREF					'	COLORREF   clrCurRowFull;       		///<突出显示当前行颜色
	clrMatchSel As COLORREF						'	COLORREF   clrMatchSel;         		///<匹配选择文本背景色
	clrAlignLine As COLORREF					'	COLORREF   clrAlignLine;        		///<对齐线
	clrAlignLineSel As COLORREF					'	COLORREF   clrAlignLineSel;     		///<对齐线 - 选择内容块
	clrFunSplitLine As COLORREF					'	COLORREF   clrFunSplitLine;     		///<函数分割线颜色 new
'	//选择文本背景 通过API设置
'	//插入符颜色   通过API设置	
	
	styleSys As DWORD							'	int       styleSys;             ///<系统关键字  return, break, for
	styleFunction As DWORD						'	int       styleFunction;        ///<函数
	styleVar As DWORD							'	int       styleVar;             ///<变量
	styleDataType As DWORD						'	int       styleDataType;        ///<基础数据类型  int, byte, char
	styleClass As DWORD'	int       styleClass;           ///<类  class
	styleMacro As DWORD'	int       styleMacro;           ///<宏
	styleEnum As DWORD'	int       styleEnum;            ///<枚举   new
	styleNumber As DWORD'	int       styleNumber;          ///<数字
	styleString As DWORD'	int       styleString;          ///<字符串
	styleComment As DWORD'	int       styleComment;         ///<注释
	StylePunctuation As DWORD'	int       StylePunctuation;     ///<标点符号  new
End Type'};

'/// 月历元素项数据
Type monthCal_item_'struct monthCal_item_'{
	nDay As Long'	int  nDay;     ///< 日期
	nType As Long'	int  nType;    ///< 1上月,2当月,3下月
	nState As Long'	int  nState;   ///< 组合状态 monthCal_state_flag_
	rcItem As RECT'	RECT rcItem;   ///< 项坐标
End Type'};
'///@}
'}

'///@defgroup  group_func_  炫彩回调函数定义
'///@{
Type funDebugError As Sub(ByVal pInfo As LPCSTR)													'typedef void (CALLBACK* funDebugError)(const char* pInfo);       //错误回调
Type funLoadFile As Function(ByVal pFileName As LPCWSTR) As BOOL									'typedef BOOL (CALLBACK* funLoadFile)(const wchar_t* pFileName);  //图片资源文件加载回调
Type funCloudEvent As Sub(ByVal pFileName As LPCWSTR,ByVal nEvent As Long,ByVal hXCGUI As HXCGUI)	'typedef void (CALLBACK* funCloudEvent)(const wchar_t* pFileName, int nEvent, HXCGUI hXCGUI);  //云UI事件回调

Type funCallUiThread As Function(ByVal nData As Integer) As Integer									'typedef vint (CALLBACK* funCallUiThread)(vint data);
Type funIdle As Sub()																				'typedef void (CALLBACK* funIdle)();
Type funExit As Sub()																				'typedef void (CALLBACK* funExit)();

Type funAnimation As Sub(ByVal hAnimation As HXCGUI,ByVal nFlag As Long)							'typedef  void(CALLBACK* funAnimation)(HXCGUI hAnimation, int flag);
Type funAnimationItem As Sub(ByVal hAnimation As HXCGUI,ByVal nPos As Single)						'typedef  void(CALLBACK* funAnimationItem)(HXCGUI hAnimation, float pos);
'///@}


Extern pLib As Any Ptr

#endif
