#ifndef __CLSXFONT_BI__
#define __CLSXFONT_BI__

#include Once "XCGUI.bi"

Type clsXFont Extends Object 
Private:    
    m_hFont As HFONTX
Public:
	Declare Function Create(ByVal nSize As Long) As HFONTX'XC_API HFONTX WINAPI XFont_Create(int size);//创建字体
	Declare Function CreateEx(ByVal pName As LPCWSTR = @WStr("宋体"),ByVal nSize As Long = 12,ByVal iStyle As Long = fontStyle_regular) As HFONTX'XC_API HFONTX WINAPI XFont_CreateEx(const wchar_t* pName = L"宋体", int size = 12, int style = fontStyle_regular);
	Declare Function CreateFromLOGFONTW(ByVal pFontInfo As LOGFONTW Ptr) As HFONTX'XC_API HFONTX WINAPI XFont_CreateFromLOGFONTW(LOGFONTW* pFontInfo);
	Declare Function CreateFromHFONT(ByVal hFont As HFONT) As HFONTX'XC_API HFONTX WINAPI XFont_CreateFromHFONT(HFONT hFont);
	Declare Function CreateFromFont(ByVal pFont As Any Ptr) As HFONTX'XC_API HFONTX WINAPI XFont_CreateFromFont(void* pFont);
	Declare Function CreateFromFile(ByVal pFontFile As LPCWSTR,ByVal nSize As Long = 12,ByVal iStyle As Long = fontStyle_regular) As HFONTX'XC_API HFONTX WINAPI XFont_CreateFromFile(const wchar_t* pFontFile, int size = 12, int style =fontStyle_regular);
	Declare Sub EnableAutoDestroy(ByVal bEnable As BOOL)'XC_API void   WINAPI XFont_EnableAutoDestroy(HFONTX hFontX, BOOL bEnable);
	Declare Function GetFont() As Any Ptr'XC_API void* WINAPI XFont_GetFont(HFONTX hFontX);
	Declare Sub GetFontInfo(ByRef pInfo As font_info_)'XC_API void  WINAPI XFont_GetFontInfo(HFONTX hFontX, out_ font_info_* pInfo);
	Declare Function GetLOGFONTW(ByVal dc As HDC,ByRef pOut As LOGFONTW) As BOOL'XC_API BOOL  WINAPI XFont_GetLOGFONTW(HFONTX hFontX, HDC hdc, out_ LOGFONTW* pOut);
	Declare Sub AddRef()'XC_API void  WINAPI XFont_AddRef(HFONTX hFontX);   //增加引用计数
	Declare Sub Release()'XC_API void  WINAPI XFont_Release(HFONTX hFontX);  //释放引用计数
	Declare Function GetRefCount() As Long'XC_API int   WINAPI XFont_GetRefCount(HFONTX hFontX);
	Declare Sub Destroy()'XC_API void  WINAPI XFont_Destroy(HFONTX hFontX);  //销毁字体	
'//v3.1.1
	Declare Function CreateFromMem(ByVal lpData As Any Ptr,ByVal length As UINT,ByVal fontsize As Long = 12,ByVal iStyle As Long = fontStyle_regular) As HFONTX'XC_API HFONTX WINAPI XFont_CreateFromMem(void* data, UINT length, int fontSize = 12, int style = fontStyle_regular);
	Declare Function CreateFromRes(ByVal id As Long,ByVal pType As LPCWSTR,ByVal fontsize As Long,ByVal istyle As Long,ByVal hModule As HMODULE = NULL) As HFONTX'XC_API HFONTX WINAPI XFont_CreateFromRes(int id, const wchar_t* pType, int fontSize, int style, HMODULE hModule=NULL);
'//3.3.5
	Declare Function CreateFromZip(ByVal pZipFileName As LPCWSTR,ByVal pFileName As LPCWSTR,ByVal pPassword As LPCWSTR,ByVal fontsize As Long,ByVal iStyle As Long) As HFONTX'XC_API HFONTX WINAPI XFont_CreateFromZip(const wchar_t* pZipFileName,const wchar_t* pFileName, const wchar_t* pPassword, int fontSize, int style);
	Declare Function CreateFromZipMem(ByVal lpData As Any Ptr,ByVal pFileName As LPCWSTR,ByVal pPassword As LPCWSTR,ByVal fontsize As Long,ByVal iStyle As Long) As HFONTX'XC_API HFONTX WINAPI XFont_CreateFromZipMem(void* data, int length, const wchar_t* pFileName, const wchar_t* pPassword, int fontSize, int style);

Public:
	Declare Property hFontHandle()As HFONTX
	Declare Property hFontHandle(ByVal hFont As HFONTX)
End Type



#endif
