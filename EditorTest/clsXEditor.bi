#ifndef __CLSXEDITOR_BI__
#define __CLSXEDITOR_BI__

#include Once "XCGUI.bi"
#include Once "clsXEdit.bi"

Type clsXEditor Extends clsXEdit
Public:
	Declare Function Create(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal hParent As HXCGUI = NULL) As HELE'XC_API HELE WINAPI XEditor_Create(int x, int y, int cx, int cy, HXCGUI hParent = NULL);
	Declare Sub EnableAutoMatchSpaseSelect(ByVal bEnable As BOOL)'XC_API void WINAPI XEidtor_EnableAutoMatchSpaseSelect(HELE hEle, BOOL bEnable);
	Declare Function IsBreakpoint(ByVal iRow As Long) As BOOL'XC_API BOOL WINAPI XEditor_IsBreakpoint(HELE hEle, int iRow);
	Declare Function SetBreakpoint(ByVal iRow As Long,ByVal bActivate As BOOL = TRUE) As BOOL'XC_API BOOL WINAPI XEditor_SetBreakpoint(HELE hEle, int iRow, BOOL bActivate = TRUE);
	Declare Function GetBreakpointCount() As Long'XC_API int  WINAPI XEditor_GetBreakpointCount(HELE hEle);
	Declare Function GetBreakpoints(aPoints() As Long,ByVal nCount As Long) As Long'XC_API int  WINAPI XEditor_GetBreakpoints(HELE hEle, out_buffer_ int* aPoints, int nCount);
	Declare Function RemoveBreakpoint(ByVal iRow As Long) As BOOL'XC_API BOOL WINAPI XEditor_RemoveBreakpoint(HELE hEle, int iRow);
	Declare Sub ClearBreakpoint()'XC_API void WINAPI XEditor_ClearBreakpoint(HELE hEle);
	Declare Function SetRunRow(ByVal iRow As Long) As BOOL'XC_API BOOL WINAPI XEditor_SetRunRow(HELE hEle, int iRow);
	Declare Sub GetColor(ByVal pInfo As editor_color_ Ptr)'XC_API void WINAPI XEditor_GetColor(HELE hEle, out_ editor_color_* pInfo);
	Declare Sub SetColor(ByVal pInfo As editor_color_ Ptr)'XC_API void WINAPI XEditor_SetColor(HELE hEle, editor_color_* pInfo);
	Declare Sub SetStyleKeyword(ByVal iStyle As Long)'XC_API void WINAPI XEditor_SetStyleKeyword(HELE hEle, int iStyle);
	Declare Sub SetStyleFunction(ByVal iStyle As Long)'XC_API void WINAPI XEditor_SetStyleFunction(HELE hEle, int iStyle);
	Declare Sub SetStyleVar(ByVal iStyle As Long)'XC_API void WINAPI XEditor_SetStyleVar(HELE hEle, int iStyle);
	Declare Sub SetStyleDataType(ByVal iStyle As Long)'XC_API void WINAPI XEditor_SetStyleDataType(HELE hEle, int iStyle);
	Declare Sub SetStyleClass(ByVal iStyle As Long)'XC_API void WINAPI XEditor_SetStyleClass(HELE hEle, int iStyle);
	Declare Sub SetStyleMacro(ByVal iStyle As Long)'XC_API void WINAPI XEditor_SetStyleMacro(HELE hEle, int iStyle);
	Declare Sub SetStyleString(ByVal iStyle As Long)'XC_API void WINAPI XEditor_SetStyleString(HELE hEle, int iStyle);
	Declare Sub SetStyleComment(ByVal iStyle As Long)'XC_API void WINAPI XEditor_SetStyleComment(HELE hEle, int iStyle);
	Declare Sub SetStyleNumber(ByVal iStyle As Long)'XC_API void WINAPI XEditor_SetStyleNumber(HELE hEle, int iStyle);
	Declare Sub SetCurRow(ByVal iRow As Long)'XC_API void WINAPI XEditor_SetCurRow(HELE hEle, int iRow);
	Declare Function GetDepth(ByVal iRow As Long) As Long'XC_API int  WINAPI XEditor_GetDepth(HELE hEle, int iRow);
	Declare Function ToExpandRow(ByVal iRow As Long) As Long'XC_API int  WINAPI XEditor_ToExpandRow(HELE hEle, int iRow);
	Declare Sub ExpandAll(ByVal bExpand As BOOL)'XC_API void WINAPI XEditor_ExpandAll(HELE hEle, BOOL bExpand);
	Declare Sub Expand(ByVal iRow As Long,ByVal bExpand As BOOL)'XC_API void WINAPI XEditor_Expand(HELE hEle, int iRow, BOOL bExpand);
	Declare Sub ExpandEx(ByVal iRow As Long)'XC_API void WINAPI XEditor_ExpandEx(HELE hEle, int iRow);
	Declare Sub AddKeyword(ByVal pKey As LPCWSTR,ByVal iStyle As Long)'XC_API void WINAPI XEditor_AddKeyword(HELE hEle, const wchar_t* pKey, int iStyle);
	Declare Sub AddConst(ByVal pKey As LPCWSTR)'XC_API void WINAPI XEditor_AddConst(HELE hEle, const wchar_t* pKey);
	Declare Sub AddFunction(ByVal pKey As LPCWSTR)'XC_API void WINAPI XEditor_AddFunction(HELE hEle, const wchar_t* pKey);
	Declare Sub AddExcludeDefVarKeyword(ByVal pKey As LPCWSTR)'XC_API void WINAPI XEditor_AddExcludeDefVarKeyword(HELE hEle, const wchar_t* pKeyword);
	Declare Sub FunArgsExpand_AddArg(ByVal pTypeName As LPCWSTR,ByVal pArgName As LPCWSTR,ByVal pText As LPCWSTR)'XC_API void WINAPI XEditor_FunArgsExpand_AddArg(HELE hEle, const wchar_t* pTypeName, const wchar_t* pArgName, const wchar_t* pText);
	Declare Sub FunArgsExpand_Expand(ByVal pTypeName As LPCWSTR,ByVal iRow As Long,ByVal iCol As Long,ByVal iCol2 As Long,ByVal nDepth As Long)'XC_API void WINAPI XEditor_FunArgsExpand_Expand(HELE hEle, const wchar_t* pFunName, int iRow, int iCol, int iCol2, int nDepth);	
'//v3.3.4
	Declare Function GetExpandState() As LPCSTR'XC_API const char* WINAPI XEditor_GetExpandState(HELE hEle); //��ȡ�۵�״̬
	Declare Function SetExpandState(ByVal pString As LPCSTR) As BOOL'XC_API BOOL WINAPI XEditor_SetExpandState(HELE hEle, const char* pString); //�����۵�״̬
	Declare Function GetIndentation(ByVal iRow As Long) As Long'XC_API int  WINAPI XEditor_GetIndentation(HELE hEle, int iRow);
	Declare Function IsEmptyRow(ByVal iRow As Long) As BOOL'XC_API BOOL WINAPI XEidtor_IsEmptyRow(HELE hEle, int iRow);
'//3.3.5	
	Declare Sub SetAutoMatchMode(ByVal nMode As Long)'XC_API void WINAPI XEditor_SetAutoMatchMode(HELE hEle, int mode);
End Type



#endif
