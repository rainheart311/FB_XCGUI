#ifndef __CLSXWND_BI__
#define __CLSXWND_BI__

#include Once "XCGUI.bi"

Type clsXWnd Extends Object
Private:    
    m_hWindow As HWINDOW
Public:
	Declare Function RegEventC(ByVal nEvent As Long,ByVal pFun As Any Ptr) As BOOL'XC_API BOOL WINAPI XWnd_RegEventC(HWINDOW hWindow, int nEvent, void* pFun);
	Declare Function RegEventC1(ByVal nEvent As Long,ByVal pFun As Any Ptr) As BOOL'XC_API BOOL WINAPI XWnd_RegEventC1(HWINDOW hWindow, int nEvent, void* pFun);
	Declare Function RemoveEventC(ByVal nEvent As Long,ByVal pFun As Any Ptr) As BOOL'XC_API BOOL WINAPI XWnd_RemoveEventC(HWINDOW hWindow, int nEvent, void* pFun);
	Declare Function Create(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal pTitle As LPCWSTR,ByVal hWndParent As HWND = NULL,ByVal XCStyle As Long = window_style_default) As HWINDOW'XC_API HWINDOW WINAPI XWnd_Create(int x, int y, int cx, int cy, const wchar_t* pTitle, HWND hWndParent = NULL, int XCStyle = window_style_default);
	Declare Function AddChild(ByVal hChild As HXCGUI) As BOOL'XC_API BOOL WINAPI XWnd_AddChild(HWINDOW hWindow, HXCGUI hChild);
	Declare Function InsertChild(ByVal hChild As HXCGUI,ByVal index As Long) As BOOL'XC_API BOOL WINAPI XWnd_InsertChild(HWINDOW hWindow, HXCGUI hChild, int index);
	Declare Function GetHWND() As HWND'XC_API HWND WINAPI XWnd_GetHWND(HWINDOW hWindow);
	Declare Sub EnableDragBorder(ByVal bEnable As BOOL)'XC_API void WINAPI XWnd_EnableDragBorder(HWINDOW hWindow, BOOL bEnable); 
	Declare Sub EnableDragWindow(ByVal bEnable As BOOL)'XC_API void WINAPI XWnd_EnableDragWindow(HWINDOW hWindow, BOOL bEnable);
	Declare Sub EnableDragCaption(ByVal bEnable As BOOL)'XC_API void WINAPI XWnd_EnableDragCaption(HWINDOW hWindow, BOOL bEnable);
	Declare Sub EnableDrawBk(ByVal bEnable As BOOL)'XC_API void WINAPI XWnd_EnableDrawBk(HWINDOW hWindow, BOOL bEnable);
	Declare Sub EnableAutoFocus(ByVal bEnable As BOOL)'XC_API void WINAPI XWnd_EnableAutoFocus(HWINDOW hWindow, BOOL bEnable); //当鼠标左键按下是否获得焦点
	Declare Sub EnableMaxWindow(ByVal bEnable As BOOL)'XC_API void WINAPI XWnd_EnableMaxWindow(HWINDOW hWindow, BOOL bEnable);
	Declare Sub EnablemLimitWindowSize(ByVal bEnable As BOOL)'XC_API void WINAPI XWnd_EnablemLimitWindowSize(HWINDOW hWindow, BOOL bEnable);
	Declare Sub EnableLayout(ByVal bEnable As BOOL)'XC_API void WINAPI XWnd_EnableLayout(HWINDOW hWindow, BOOL bEnable);
	Declare Sub EnableLayoutOverlayBorder(ByVal bEnable As BOOL)'XC_API void WINAPI XWnd_EnableLayoutOverlayBorder(HWINDOW hWindow, BOOL bEnable);
	Declare Sub ShowLayoutFrame(ByVal bEnable As BOOL)'XC_API void WINAPI XWnd_ShowLayoutFrame(HWINDOW hWindow, BOOL bEnable);
	Declare Function IsEnableLayout() As BOOL'XC_API BOOL WINAPI XWnd_IsEnableLayout(HWINDOW hWindow);
	Declare Function IsMaxWindow() As BOOL'XC_API BOOL WINAPI XWnd_IsMaxWindow(HWINDOW hWindow);
	Declare Sub Redraw(ByVal bImmediate As BOOL = FALSE)'XC_API void WINAPI XWnd_Redraw(HWINDOW hWindow, BOOL bImmediate = FALSE);
	Declare Sub RedrawRect(ByVal pRect As RECT Ptr,ByVal bImmediate As BOOL = FALSE)'XC_API void WINAPI XWnd_RedrawRect(HWINDOW hWindow, RECT* pRect, BOOL bImmediate = FALSE); //重绘窗口指定区域
	Declare Sub SetFocusEle(ByVal hFocusEle As HELE)'XC_API void WINAPI XWnd_SetFocusEle(HWINDOW hWindow, HELE hFocusEle);
	Declare Function GetFocusEle() As HELE'XC_API HELE WINAPI XWnd_GetFocusEle(HWINDOW hWindow);
	Declare Function GetStayEle() As HELE'XC_API HELE WINAPI XWnd_GetStayEle(HWINDOW hWindow);  //获取当前鼠标所停留元素
	Declare Sub DrawWindow(ByVal hDraw As HDRAW)'XC_API void WINAPI XWnd_DrawWindow(HWINDOW hWindow, HDRAW hDraw);
	Declare Sub Center()'XC_API void WINAPI XWnd_Center(HWINDOW hWindow);
	Declare Sub CenterEx(ByVal nWidth As Long,ByVal nHeight As Long)'XC_API void WINAPI XWnd_CenterEx(HWINDOW hWindow, int width, int height);
	Declare Sub SetCursor(ByVal hCur As HCURSOR)'XC_API void WINAPI XWnd_SetCursor(HWINDOW hWindow, HCURSOR hCursor);
	Declare Function GetCursor() As HCURSOR'XC_API HCURSOR WINAPI XWnd_GetCursor(HWINDOW hWindow);
	Declare Function SetCursorSys(ByVal hCur As HCURSOR) As HCURSOR'XC_API HCURSOR WINAPI XWnd_SetCursorSys(HWINDOW hWindow, HCURSOR hCursor);
	Declare Sub SetFont(ByVal hFont As HFONTX)'XC_API void WINAPI XWnd_SetFont(HWINDOW hWindow, HFONTX hFontx);
	Declare Sub SetTextColor(ByVal nColor As COLORREF)'XC_API void WINAPI XWnd_SetTextColor(HWINDOW hWindow, COLORREF color);
	Declare Function GetTextColor() As COLORREF'XC_API COLORREF WINAPI XWnd_GetTextColor(HWINDOW hWindow);
	Declare Function GetTextColorEx() As COLORREF'XC_API COLORREF WINAPI XWnd_GetTextColorEx(HWINDOW hWindow);
	Declare Sub SetID(ByVal nID As Long)'XC_API void WINAPI XWnd_SetID(HWINDOW hWindow, int nID);
	Declare Function GetID() As Long'XC_API int  WINAPI XWnd_GetID(HWINDOW hWindow);
	Declare Sub SetName(ByVal pName As LPCWSTR)'XC_API void  WINAPI XWnd_SetName(HWINDOW hWindow, const wchar_t* pName);
	Declare Function GetName() As LPCWSTR'XC_API const wchar_t* WINAPI XWnd_GetName(HWINDOW hWindow);
	Declare Sub SetCaptureEle(ByVal hEle As HELE)'XC_API void WINAPI XWnd_SetCaptureEle(HWINDOW hWindow, HELE hEle); 
	Declare Function GetCaptureEle() As HELE'XC_API HELE WINAPI XWnd_GetCaptureEle(HWINDOW hWindow);
	Declare Sub SetBorderSize(ByVal nLeft As Long,ByVal nTop As Long,ByVal nRight As Long,ByVal nBottom As Long)'XC_API void WINAPI XWnd_SetBorderSize(HWINDOW hWindow, int left, int top, int right, int bottom);
	Declare Sub GetBorderSize(ByRef pBorder As borderSize_)'XC_API void WINAPI XWnd_GetBorderSize(HWINDOW hWindow, out_ borderSize_* pBorder);
	Declare Sub SetPadding(ByVal nLeft As Long,ByVal nTop As Long,ByVal nRight As Long,ByVal nBottom As Long)'XC_API void WINAPI XWnd_SetPadding(HWINDOW hWindow, int left, int top, int right, int bottom);
	Declare Sub GetPadding(ByRef pPadding As paddingSize_)'XC_API void WINAPI XWnd_GetPadding(HWINDOW hWindow, out_ paddingSize_* pPadding);
	Declare Sub SetDragBorderSize(ByVal nLeft As Long,ByVal nTop As Long,ByVal nRight As Long,ByVal nBottom As Long)'XC_API void WINAPI XWnd_SetDragBorderSize(HWINDOW hWindow, int left, int top, int right, int bottom); //设置拖动边框大小
	Declare Sub GetDragBorderSize(ByRef pSize As borderSize_)'XC_API void WINAPI XWnd_GetDragBorderSize(HWINDOW hWindow, out_ borderSize_* pSize);
	Declare Sub SetMinimumSize(ByVal nWidth As Long,ByVal nHeight As Long)'XC_API void WINAPI XWnd_SetMinimumSize(HWINDOW hWindow, int width, int height);
	Declare Function HitChildEle(ByVal lppt As LPPOINT) As HELE'XC_API HELE WINAPI XWnd_HitChildEle(HWINDOW hWindow, POINT* pPt);
	Declare Function GetChildCount() As Long'XC_API int  WINAPI XWnd_GetChildCount(HWINDOW hWindow);
	Declare Function GetChildByIndex(ByVal index As Long) As HXCGUI'XC_API HXCGUI WINAPI XWnd_GetChildByIndex(HWINDOW hWindow, int index);
	Declare Function GetChildByID(ByVal nID As Long) As HXCGUI'XC_API HXCGUI WINAPI XWnd_GetChildByID(HWINDOW hWindow, int nID);
	Declare Function GetChild(ByVal nID As Long) As HXCGUI'XC_API HXCGUI WINAPI XWnd_GetChild(HWINDOW hWindow, int nID);
	Declare Sub GetDrawRect(ByVal pRCPaint As LPRECT)'XC_API void WINAPI XWnd_GetDrawRect(HWINDOW hWindow, RECT* pRcPaint);
	Declare Function ShowWindow(ByVal nCmdShow As Long) As BOOL'XC_API BOOL WINAPI XWnd_ShowWindow(HWINDOW hWindow, int nCmdShow);
	Declare Sub AdjustLayout()'XC_API void WINAPI XWnd_AdjustLayout(HWINDOW hWindow);
	Declare Sub AdjustLayoutEx(ByVal nFlags As Long = adjustLayout_self)'XC_API void WINAPI XWnd_AdjustLayoutEx(HWINDOW hWindow, int nFlags = adjustLayout_self);
	Declare Sub CloseWindow()'XC_API void WINAPI XWnd_CloseWindow(HWINDOW hWindow);
	Declare Sub CreateCaret(ByVal hEle As HELE,ByVal x As Long,ByVal y As Long,ByVal nWidth As Long,ByVal nHeight As Long)'XC_API void WINAPI XWnd_CreateCaret(HWINDOW hWindow, HELE hEle, int x, int y, int width, int height);//创建插入符
	Declare Function GetCaretHELE() As HELE'XC_API HELE WINAPI XWnd_GetCaretHELE(HWINDOW hWindow);
	Declare Sub SetCaretColor(ByVal nColor As COLORREF)'XC_API void WINAPI XWnd_SetCaretColor(HWINDOW hWindow, COLORREF color); //设置插入符颜色
	Declare Sub ShowCaret(ByVal bShow As BOOL)'XC_API void WINAPI XWnd_ShowCaret(HWINDOW hWindow, BOOL bShow);  //显示插入符
	Declare Sub DestroyCaret()'XC_API void WINAPI XWnd_DestroyCaret(HWINDOW hWindow);    //销毁插入符
	Declare Sub SetCaretPos(ByVal x As Long,ByVal y As Long,ByVal nWidth As Long,ByVal nHeight As Long,ByVal bUpdate As BOOL = FALSE)'XC_API void WINAPI XWnd_SetCaretPos(HWINDOW hWindow, int x, int y, int width, int height, BOOL bUpdate = FALSE);
	Declare Function GetCaretInfo(ByRef x As Long,ByRef y As Long,ByRef nWidth As Long,ByRef nHeight As Long) As HELE'XC_API HELE WINAPI XWnd_GetCaretInfo(HWINDOW hWindow, int* pX, int* pY, int* pWidth, int* pHeight);
	Declare Function GetClientRect(ByRef pRect As LPRECT) As BOOL'XC_API BOOL WINAPI XWnd_GetClientRect(HWINDOW hWindow, out_ RECT* pRect); //获取客户区坐标
	Declare Sub GetBodyRect(ByRef pRect As LPRECT)'XC_API void WINAPI XWnd_GetBodyRect(HWINDOW hWindow, out_ RECT* pRect);  //获取窗口body坐标
	Declare Sub GetLayoutRect(ByRef pRect As LPRECT)'XC_API void WINAPI XWnd_GetLayoutRect(HWINDOW hWindow, out_ RECT* pRect);
	Declare Sub GetRect(ByRef pRect As LPRECT)'XC_API void WINAPI XWnd_GetRect(HWINDOW hWindow, out_ RECT* pRect);
	Declare Sub SetRect(ByRef pRect As LPRECT)'XC_API void WINAPI XWnd_SetRect(HWINDOW hWindow, RECT* pRect);
	Declare Sub SetTop()'XC_API void WINAPI XWnd_SetTop(HWINDOW hWindow);
	Declare Sub MaxWindow(ByVal bMaximize As BOOL)'XC_API void WINAPI XWnd_MaxWindow(HWINDOW hWindow, BOOL bMaximize);
	Declare Function SetTimer(ByVal nIDEvent As UINT,ByVal uElapse As UINT) As UINT'XC_API UINT WINAPI XWnd_SetTimer(HWINDOW hWindow, UINT nIDEvent, UINT uElapse); //设置定时器
	Declare Function KillTimer(ByVal nIDEvent As UINT) As BOOL'XC_API BOOL WINAPI XWnd_KillTimer(HWINDOW hWindow, UINT nIDEvent);
	Declare Function SetXCTimer(ByVal nIDEvent As UINT,ByVal uElapse As UINT) As BOOL'XC_API BOOL WINAPI XWnd_SetXCTimer(HWINDOW hWindow, UINT nIDEvent, UINT uElapse); //设置定时器
	Declare Function KillXCTimer(ByVal nIDEvent As UINT) As BOOL'XC_API BOOL WINAPI XWnd_KillXCTimer(HWINDOW hWindow, UINT nIDEvent);
	Declare Function GetBkManager() As HBKM'XC_API HBKM WINAPI  XWnd_GetBkManager(HWINDOW hWindow);
	Declare Function GetBkManagerEx() As HBKM'XC_API HBKM WINAPI  XWnd_GetBkManagerEx(HWINDOW hWindow);
	Declare Sub SetBkMagager(ByVal hBkInfoM As HBKM)'XC_API void WINAPI  XWnd_SetBkMagager(HWINDOW hWindow, HBKM hBkInfoM);
	Declare Sub SetTransparentType(ByVal nType As window_transparent_)'XC_API void WINAPI XWnd_SetTransparentType(HWINDOW hWindow, window_transparent_ nType);  //设置透明窗口
	Declare Sub SetTransparentAlpha(ByVal nAlpha As UByte)'XC_API void WINAPI XWnd_SetTransparentAlpha(HWINDOW hWindow, BYTE alpha); //设置窗口透明度
	Declare Sub SetTransparentColor(ByVal nColor As COLORREF)'XC_API void WINAPI XWnd_SetTransparentColor(HWINDOW hWindow, COLORREF color); //设置窗口透明色
	Declare Sub SetShadowInfo(ByVal nSize As Long,ByVal nDepth As Long,ByVal nAngeleSize As Long,ByVal bRightAngle As BOOL,ByVal nColor As COLORREF)'XC_API void WINAPI XWnd_SetShadowInfo(HWINDOW hWindow, int nSize, int nDepth, int nAngeleSize, BOOL bRightAngle, COLORREF color);
	Declare Function GetTransparentType() As window_transparent_'XC_API window_transparent_ WINAPI XWnd_GetTransparentType(HWINDOW hWindow);
	Declare Sub GetShadowInfo(ByRef nSize As Long,ByRef nDepth As Long,ByRef nAngeleSize As Long,ByRef bRightAngle As BOOL,ByRef nColor As COLORREF)'XC_API void WINAPI XWnd_GetShadowInfo(HWINDOW hWindow, out_ int* pnSize, out_ int* pnDepth, out_ int* pnAngeleSize, out_ BOOL* pbRightAngle, out_ COLORREF* pColor);
	Declare Function Attach(ByVal hWnd As HWND,ByVal XCStyle As Long) As HWINDOW'XC_API HWINDOW WINAPI XWnd_Attach(HWND hWnd, int XCStyle);
	Declare Sub EnableDragFiles(ByVal bEnable As BOOL)'XC_API void WINAPI XWnd_EnableDragFiles(HWINDOW hWindow, BOOL bEnable);
	Declare Sub Show(ByVal bShow As BOOL)'XC_API void WINAPI XWnd_Show(HWINDOW hWindow, BOOL bShow);
'//v3.1.1
	Declare Sub SetIcon(ByVal hImage As HIMAGE)'XC_API void WINAPI XWnd_SetIcon(HWINDOW hWindow, HIMAGE hImage);
	Declare Sub SetTitle(ByVal pTitle As LPCWSTR)'XC_API void WINAPI XWnd_SetTitle(HWINDOW hWindow, const wchar_t* pTitle);
	Declare Sub SetTitleColor(ByVal nColor As COLORREF)'XC_API void WINAPI XWnd_SetTitleColor(HWINDOW hWindow, COLORREF color);
	Declare Function GetButton(ByVal nFlag As Long) As HELE'XC_API HELE WINAPI XWnd_GetButton(HWINDOW hWindow, int nFlag);
	Declare Function GetIcon() As HIMAGE'XC_API HIMAGE WINAPI XWnd_GetIcon(HWINDOW hWindow);
	Declare Function GetTitle() As LPCWSTR'XC_API const wchar_t* WINAPI XWnd_GetTitle(HWINDOW hWindow);
	Declare Function GetTitleColor() As COLORREF'XC_API COLORREF WINAPI XWnd_GetTitleColor(HWINDOW hWindow);
'//3.2.0
	Declare Sub AddBkBorder(ByVal nState As Long,ByVal nColor As COLORREF,ByVal nWidth As Long)'XC_API void WINAPI XWnd_AddBkBorder(HWINDOW hWindow, int nState, COLORREF color, int width);
	Declare Sub AddBkFill(ByVal nState As Long,ByVal nColor As COLORREF)'XC_API void WINAPI XWnd_AddBkFill(HWINDOW hWindow, int nState, COLORREF color);
	Declare Sub AddBkImage(ByVal nState As Long,ByVal hImage As HIMAGE)'XC_API void WINAPI XWnd_AddBkImage(HWINDOW hWindow, int nState, HIMAGE hImage);
	Declare Function GetBkInfoCount() As Long'XC_API int  WINAPI XWnd_GetBkInfoCount(HWINDOW hWindow);
	Declare Sub ClearBkInfo()'XC_API void WINAPI XWnd_ClearBkInfo(HWINDOW hWindow);
'//3.3.0
	Declare Sub SetPosition(ByVal x As Long,ByVal y As Long)'XC_API void WINAPI XWnd_SetPosition(HWINDOW hWindow, int x, int y);
'//v3.3.1
	Declare Function CreateEx(ByVal dwExStyle As DWORD,ByVal dwStyle As DWORD,ByVal lpClassName As LPCWSTR,ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal pTitle As LPCWSTR,ByVal hWndParent As HWND = NULL,ByVal XCStyle As Long = window_style_default) As HWINDOW'XC_API HWINDOW WINAPI XWnd_CreateEx(DWORD dwExStyle, DWORD dwStyle, const wchar_t* lpClassName, int x, int y, int cx, int cy, const wchar_t* pTitle, HWND hWndParent = NULL, int XCStyle = window_style_default);
'//v3.3.2
	Declare Function SetBkInfo(ByVal pText As LPCWSTR) As Long'XC_API int  WINAPI XWnd_SetBkInfo(HWINDOW hWindow, const wchar_t* pText);
	Declare Sub SetCaptionMargin(ByVal nLeft As Long,ByVal nTop As Long,ByVal nRight As Long,ByVal nBottom As Long)'XC_API void WINAPI XWnd_SetCaptionMargin(HWINDOW hWindow, int left, int top, int right, int bottom);
	Declare Function IsDragBorder() As BOOL'XC_API BOOL WINAPI XWnd_IsDragBorder(HWINDOW hWindow);
	Declare Function IsDragWindow() As BOOL'XC_API BOOL WINAPI XWnd_IsDragWindow(HWINDOW hWindow);
	Declare Function IsDragCaption() As BOOL'XC_API BOOL WINAPI XWnd_IsDragCaption(HWINDOW hWindow);
Public:
	Declare Property hWndHandle() As HWINDOW
End Type

'XC_API BOOL WINAPI _XWnd_RegEvent(HWINDOW hWindow, UINT nEvent, xc_event* pEvent);
'XC_API BOOL WINAPI _XWnd_RemoveEvent(HWINDOW hWindow, UINT nEvent, xc_event* pEvent);
'XC_API BOOL WINAPI _XWnd_RegEvent(HWINDOW hWindow, UINT nEvent, xc_event* pEvent);
'XC_API BOOL WINAPI _XWnd_RemoveEvent(HWINDOW hWindow, UINT nEvent, xc_event* pEvent);	

#endif
