#include Once "windows.bi"
#include Once "clsXEle.bi"

'XC_API HELE WINAPI XEle_Create(int x, int y, int cx, int cy, HXCGUI hParent = NULL);
Function clsXEle.Create(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal hParent As HXCGUI = NULL) As HELE
	If pLib Then   
        Dim pFunc As Function(ByVal As Long,ByVal As Long,ByVal As Long,ByVal As Long,ByVal As HXCGUI) As HELE
        pFunc = DyLibSymbol(pLib,"XEle_Create") 
        If pFunc Then 
            m_hEle = pFunc(x,y,cx,cy,hParent)
        Else
    		Print "XEle_Create函数不存在"
        End If
	Else
    	Print "DLL不存在"
	End If
	Return m_hEle
End Function

'XC_API void WINAPI XEle_EnableKeyTab(HELE hEle, BOOL bEnable);
Sub clsXEle.EnableKeyTab(ByVal bEnable As BOOL)
	If pLib Then   
        Dim pFunc As Sub(ByVal As HELE,ByVal As BOOL)
        pFunc = DyLibSymbol(pLib,"XEle_EnableKeyTab") 
        If pFunc Then 
            pFunc(m_hEle,bEnable)
        Else
    		Print "XEle_EnableKeyTab函数不存在"
        End If
	Else
    	Print "DLL不存在"
    End If    
End Sub

'XC_API void WINAPI XEle_SetFont(HELE hEle, HFONTX hFontx);
Sub clsXEle.SetFont(ByVal hFont As HFONTX)
	If pLib Then   
        Dim pFunc As Sub(ByVal As HELE,ByVal As HFONTX)
        pFunc = DyLibSymbol(pLib,"XEle_SetFont") 
        If pFunc Then 
            pFunc(m_hEle,hFont)
        Else
    		Print "XEle_SetFont函数不存在"
        End If
	Else
    	Print "DLL不存在"
    End If    
End Sub

'XC_API void WINAPI XEle_SetTextColor(HELE hEle, COLORREF color); 
Sub clsXEle.SetTextColor(ByVal nColor As COLORREF)
	If pLib Then   
        Dim pFunc As Sub(ByVal As HELE,ByVal As COLORREF)
        pFunc = DyLibSymbol(pLib,"XEle_SetTextColor") 
        If pFunc Then 
            pFunc(m_hEle,nColor)
        Else
    		Print "XEle_SetTextColor函数不存在"
        End If
	Else
    	Print "DLL不存在"
    End If    
End Sub

Property clsXEle.hEleHandle()As HELE
	Return m_hEle
End Property

Property clsXEle.hEleHandle(ByVal hEle As HELE)
	m_hEle = hEle
End Property



