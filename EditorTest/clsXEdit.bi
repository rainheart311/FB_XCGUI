#ifndef __CLSXEDIT_BI__
#define __CLSXEDIT_BI__

#include Once "XCGUI.bi"
#include Once "clsXEle.bi"

Type clsXEdit Extends clsXEle
Public:
	Declare Function Create(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal hParent As HXCGUI = NULL) As HELE'XC_API HELE WINAPI XEdit_Create(int x, int y, int cx, int cy, HXCGUI hParent = NULL);
	Declare Function CreateEx(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal nType As edit_type_,ByVal hParent As HXCGUI = NULL) As HELE'XC_API HELE WINAPI XEdit_CreateEx(int x, int y, int cx, int cy, edit_type_ type, HXCGUI hParent = NULL);
	Declare Sub EnableAutoWrap(ByVal bEnable As BOOL)'XC_API void WINAPI XEdit_EnableAutoWrap(HELE hEle, BOOL bEnable);
	Declare Sub EnableReadOnly(ByVal bEnable As BOOL)'XC_API void WINAPI XEdit_EnableReadOnly(HELE hEle, BOOL bEnable);
	Declare Sub EnableMultiLine(ByVal bEnable As BOOL)'XC_API void WINAPI XEdit_EnableMultiLine(HELE hEle, BOOL bEnable);
	Declare Sub EnablePassword(ByVal bEnable As BOOL)'XC_API void WINAPI XEdit_EnablePassword(HELE hEle, BOOL bEnable);
	Declare Sub EnableAutoSelAll(ByVal bEnable As BOOL)'XC_API void WINAPI XEdit_EnableAutoSelAll(HELE hEle, BOOL bEnable);
	Declare Sub EnableAutoCancelSel(ByVal bEnable As BOOL)'XC_API void WINAPI XEdit_EnableAutoCancelSel(HELE hEle, BOOL bEnable);
	Declare Function IsReadOnly() As BOOL'XC_API BOOL WINAPI XEdit_IsReadOnly(HELE hEle);
	Declare Function IsMultiLine() As BOOL'XC_API BOOL WINAPI XEdit_IsMultiLine(HELE hEle);
	Declare Function IsPassword() As BOOL'XC_API BOOL WINAPI XEdit_IsPassword(HELE hEle);
	Declare Function IsAutoWrap() As BOOL'XC_API BOOL WINAPI XEdit_IsAutoWrap(HELE hEle);
	Declare Function IsEmpty() As BOOL'XC_API BOOL WINAPI XEdit_IsEmpty(HELE hEle);
	Declare Function IsInSelect(ByVal iRow As Long,ByVal iCol As Long) As BOOL'XC_API BOOL WINAPI XEdit_IsInSelect(HELE hEle, int iRow, int iCol);
	Declare Function GetRowCount() As Long'XC_API int  WINAPI XEdit_GetRowCount(HELE hEle);
	Declare Function GetData() As edit_data_copy_ Ptr'XC_API edit_data_copy_* WINAPI XEdit_GetData(HELE hEle);
	Declare Sub AddData(ByVal pData As edit_data_copy_ Ptr,ByVal styleTable As UShort Ptr,ByVal nStyleCount As Long)'XC_API void WINAPI XEdit_AddData(HELE hEle, edit_data_copy_* pData, in_buffer_ USHORT* styleTable, int nStyleCount);
	Declare Sub FreeData(ByVal pData As edit_data_copy_ Ptr)'XC_API void WINAPI XEdit_FreeData(edit_data_copy_* pData);
	Declare Sub SetDefaultText(ByVal pString As LPCWSTR)'XC_API void WINAPI XEdit_SetDefaultText(HELE hEle, const wchar_t* pString);
	Declare Sub SetDefaultTextColor(ByVal nColor As COLORREF)'XC_API void WINAPI XEdit_SetDefaultTextColor(HELE hEle, COLORREF color);
	Declare Sub SetPasswordCharacter(ByVal ch As wchar_t)'XC_API void WINAPI XEdit_SetPasswordCharacter(HELE hEle, wchar_t  ch);
	Declare Sub SetTextAlign(ByVal align As Long)'XC_API void WINAPI XEdit_SetTextAlign(HELE hEle, int  align);
	Declare Sub SetTabSpace(ByVal nSpace As Long)'XC_API void WINAPI XEdit_SetTabSpace(HELE hEle, int  nSpace);
	Declare Sub SetText(ByVal pString As LPCWSTR)'XC_API void WINAPI XEdit_SetText(HELE hEle, const wchar_t* pString);
	Declare Sub SetTextInt(ByVal nValue As Long)'XC_API void WINAPI XEdit_SetTextInt(HELE hEle, int nValue);
	Declare Function GetText(ByRef pOut As WString,ByVal nOutlen As Long) As Long'XC_API int  WINAPI XEdit_GetText(HELE hEle, out_ wchar_t* pOut, int nOutlen);
	Declare Function GetTextRow(ByVal iRow As Long,ByRef pOut As WString,ByVal nOutlen As Long) As Long'XC_API int  WINAPI XEdit_GetTextRow(HELE hEle, int iRow, out_ wchar_t* pOut, int nOutlen);
	Declare Function GetLength() As Long'XC_API int  WINAPI XEdit_GetLength(HELE hEle);
	Declare Function GetLengthRow(ByVal iRow As Long) As Long'XC_API int  WINAPI XEdit_GetLengthRow(HELE hEle, int iRow);
	Declare Function GetAt(ByVal iRow As Long,ByVal iCol As Long) As wchar_t'XC_API wchar_t WINAPI XEdit_GetAt(HELE hEle, int iRow, int iCol);
	Declare Sub InsertText(ByVal iRow As Long,ByVal iCol As Long,ByVal pString As LPCWSTR)'XC_API void WINAPI XEdit_InsertText(HELE hEle, int iRow, int iCol, const wchar_t* pString);
'//XC_API void WINAPI XEdit_InsertTextUser(HELE hEle, const wchar_t* pString);
	Declare Sub AddText(ByVal pString As LPCWSTR)'XC_API void WINAPI XEdit_AddText(HELE hEle, const wchar_t* pString);
	Declare Sub AddTextEx(ByVal pString As LPCWSTR,ByVal iStyle As Long)'XC_API void WINAPI XEdit_AddTextEx(HELE hEle, const wchar_t* pString, int iStyle);
	Declare Function AddObject(ByVal hObj As HXCGUI) As Long'XC_API int  WINAPI XEdit_AddObject(HELE hEle, HXCGUI hObj);
	Declare Sub AddByStyle(ByVal iStyle As Long)'XC_API void WINAPI XEdit_AddByStyle(HELE hEle, int iStyle);
	Declare Function AddStyle(ByVal hFont_image_Obj As HXCGUI,ByVal nColor As COLORREF,ByVal bColor As BOOL) As Long'XC_API int  WINAPI XEdit_AddStyle(HELE hEle, HXCGUI hFont_image_Obj, COLORREF color, BOOL bColor);
	Declare Function AddStyleEx(ByVal fontName As LPCWSTR,ByVal nColor As COLORREF,ByVal bColor As BOOL) As Long'XC_API int  WINAPI XEdit_AddStyleEx(HELE hEle, const wchar_t* fontName, int fontSize, int fontStyle, COLORREF color, BOOL bColor);
	Declare Function GetStyleInfo(ByVal iStyle As Long,ByVal info As edit_style_info_ Ptr) As BOOL'XC_API BOOL WINAPI XEdit_GetStyleInfo(HELE hEle, int iStyle, out_ edit_style_info_* info);
	Declare Sub SetCurStyle(ByVal iStyle As Long)'XC_API void WINAPI XEdit_SetCurStyle(HELE hEle, int iStyle);
	Declare Sub SetCaretColor(ByVal nColor As COLORREF)'XC_API void WINAPI XEdit_SetCaretColor(HELE hEle, COLORREF  color);
	Declare Sub SetCaretWidth(ByVal nWidth As Long)'XC_API void WINAPI XEdit_SetCaretWidth(HELE hEle, int nWidth);
	Declare Sub SetSelectBkColor(ByVal nColor As COLORREF)'XC_API void WINAPI XEdit_SetSelectBkColor(HELE hEle, COLORREF color);

	Declare Sub SetRowHeight(ByVal nHeight As Long)'XC_API void WINAPI XEdit_SetRowHeight(HELE hEle, int nHeight);
	Declare Sub SetRowHeightEx(ByVal iRow As Long,ByVal nHeight As Long)'XC_API void WINAPI XEdit_SetRowHeightEx(HELE hEle, int iRow, int nHeight);
'//XC_API void WINAPI XEdit_SetCurPos(HELE hEle, int iRow, int iCol);
	Declare Function GetCurPos() As Long'XC_API int  WINAPI XEdit_GetCurPos(HELE hEle);
	Declare Function GetCurRow() As Long'XC_API int  WINAPI XEdit_GetCurRow(HELE hEle);
	Declare Function GetCurCol() As Long'XC_API int  WINAPI XEdit_GetCurCol(HELE hEle);
	Declare Sub GetPoint(ByVal iRow As Long,ByVal iCol As Long,ByRef pOut As Point)'XC_API void WINAPI XEdit_GetPoint(HELE hEle, int iRow, int iCol, out_ POINT* pOut);
	Declare Function AutoScroll() As BOOL'XC_API BOOL WINAPI XEdit_AutoScroll(HELE hEle);
	Declare Function AutoScrollEx(ByVal iRow As Long,ByVal iCol As Long) As BOOL'XC_API BOOL WINAPI XEdit_AutoScrollEx(HELE hEle, int iRow, int iCol);
'//XC_API void WINAPI XEdit_PositionToInfo(HELE hEle, int iPos, position_* pInfo);
	Declare Function SelectAll() As BOOL'XC_API BOOL WINAPI XEdit_SelectAll(HELE hEle);
	Declare Function CancelSelect() As BOOL'XC_API BOOL WINAPI XEdit_CancelSelect(HELE hEle);
	Declare Function DeleteSelect() As BOOL'XC_API BOOL WINAPI XEdit_DeleteSelect(HELE hEle);
	Declare Function SetSelect(ByVal iStartRow As Long,ByVal iStartCol As Long,ByVal iEndRow As Long,ByVal iEndCol As Long) As BOOL'XC_API BOOL WINAPI XEdit_SetSelect(HELE hEle, int iStartRow, int iStartCol, int iEndRow, int iEndCol);
	Declare Function GetSelectText(ByRef pOut As WString,ByVal nOutLen As Long) As Long'XC_API int  WINAPI XEdit_GetSelectText(HELE hEle, out_ wchar_t* pOut, int nOutLen);
	Declare Function GetSelectRange(ByRef pBegin As position_,ByRef pEnd As position_) As BOOL'XC_API BOOL WINAPI XEdit_GetSelectRange(HELE hEle, out_ position_* pBegin, out_ position_* pEnd);
	Declare Sub GetVisibleRowRange(ByRef iStart As Long,ByRef iEnd As Long)'XC_API void WINAPI XEdit_GetVisibleRowRange(HELE hEle, out_ int* piStart, out_ int* piEnd);
	Declare Function Delete_(ByVal iStartRow As Long,ByVal iStartCol As Long,ByVal iEndRow As Long,ByVal iEndCol As Long) As BOOL'XC_API BOOL WINAPI XEdit_Delete(HELE hEle, int iStartRow, int iStartCol, int iEndRow, int iEndCol);
	Declare Function DeleteRow(ByVal iRow As Long) As BOOL'XC_API BOOL WINAPI XEdit_DeleteRow(HELE hEle, int iRow);
	Declare Function ClipboardCut() As BOOL'XC_API BOOL WINAPI XEdit_ClipboardCut(HELE hEle);
	Declare Function ClipboardCopy() As BOOL'XC_API BOOL WINAPI XEdit_ClipboardCopy(HELE hEle);
	Declare Function ClipboardPaste() As BOOL'XC_API BOOL WINAPI XEdit_ClipboardPaste(HELE hEle);
	Declare Function Undo() As BOOL'XC_API BOOL WINAPI XEdit_Undo(HELE hEle);
	Declare Function Redo() As BOOL'XC_API BOOL WINAPI XEdit_Redo(HELE hEle);
	Declare Sub AddChatBegin(ByVal hImageAvatar As HIMAGE,ByVal hImageBubble As HIMAGE,ByVal nFlag As Long)'XC_API void WINAPI XEdit_AddChatBegin(HELE hEle, HIMAGE hImageAvatar, HIMAGE hImageBubble, int nFlag);
	Declare Sub AddChatEnd()'XC_API void WINAPI XEdit_AddChatEnd(HELE hEle);
	Declare Sub SetChatIndentation(ByVal nIndentation As Long)'XC_API void WINAPI XEdit_SetChatIndentation(HELE hEle, int nIndentation);
	Declare Function CommentSelect() As BOOL'XC_API BOOL WINAPI XEdit_CommentSelect(HELE hEle);
	Declare Function IndentationSelect(ByVal bAdd As Long) As BOOL'XC_API BOOL WINAPI XEdit_IndentationSelect(HELE hEle, int bAdd);	
'//v3.3.4
	Declare Sub SetRowSpace(ByVal nSpace As Long)'XC_API void WINAPI XEdit_SetRowSpace(HELE hEle, int nSpace);
	Declare Sub SetBackFont(ByVal hFont As HFONTX)'XC_API void WINAPI XEdit_SetBackFont(HELE hEle, HFONTX hFont);
'
	Declare Function ReleaseStyle(ByVal iStyle As Long) As BOOL'XC_API BOOL WINAPI XEdit_ReleaseStyle(HELE hEle, int iStyle);
	Declare Function ModifyStyle(ByVal hFont As HFONTX,ByVal nColor As COLORREF,ByVal bColor As BOOL) As Long'XC_API int  WINAPI XEdit_ModifyStyle(HELE hEle, HFONTX hFont, COLORREF color, BOOL bColor);
'
	Declare Sub SetSpaceSize(ByVal nSize As Long)'XC_API void WINAPI XEdit_SetSpaceSize(HELE hEle, int size);
	Declare Sub SetCharSpaceSize(ByVal nSize As Long,ByVal sizeZh As Long)'XC_API void WINAPI XEdit_SetCharSpaceSize(HELE hEle, int size, int sizeZh);
'
	Declare Function GetSelectTextLength() As Long'XC_API int  WINAPI XEdit_GetSelectTextLength(HELE hEle);
	Declare Sub SetSelectTextStyle(ByVal iStyle As Long)'XC_API void WINAPI XEdit_SetSelectTextStyle(HELE hEle, int iStyle);

	Declare Sub AddTextUser(ByVal pString As LPCWSTR)'XC_API void WINAPI XEdit_AddTextUser(HELE hEle, const wchar_t* pString);
	Declare Sub PosToRowCol(ByVal iPos As Long,ByRef pInfo As position_)'XC_API void WINAPI XEdit_PosToRowCol(HELE hEle, int iPos, out_ position_* pInfo);
	Declare Function RowColToPos(ByVal iRow As Long,ByVal iCol As Long) As Long'XC_API int  WINAPI XEdit_RowColToPos(HELE hEle, int iRow, int iCol);
	Declare Sub SetCurPosEx(ByVal iRow As Long,ByVal iCol As Long)'XC_API void WINAPI XEdit_SetCurPosEx(HELE hEle, int iRow, int iCol); //新增
	Declare Sub GetCurPosEx(ByRef iRow As Long,ByRef iCol As Long)'XC_API void WINAPI XEdit_GetCurPosEx(HELE hEle, out_ int* iRow, out_ int* iCol); //新增
	Declare Function SetCurPos(ByVal nPos As Long) As BOOL'XC_API BOOL WINAPI XEdit_SetCurPos(HELE hEle, int pos);  //修改
	Declare Sub MoveEnd()'XC_API void WINAPI XEdit_MoveEnd(HELE hEle); //新增
End Type

Type clsXEditColor Extends Object
Private:    
    m_hEle As HELE
Public:
	Declare Function Create(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal hParent As HXCGUI) As HELE'XC_API HELE WINAPI XEditColor_Create(int x, int y, int cx, int cy, HXCGUI hParent);
	Declare Sub SetColor(ByVal nColor As COLORREF)'XC_API void WINAPI XEditColor_SetColor(HELE hEle, COLORREF color);//设置颜色
	Declare Function GetColor() As COLORREF'XC_API COLORREF WINAPI XEditColor_GetColor(HELE hEle); //获取颜色RGB值	
	
Public:
	Declare Property hEleHandle()As HELE
	Declare Property hEleHandle(ByVal hEle As HELE)
End Type

Type clsXEditSet Extends Object
Private:    
    m_hEle As HELE
Public:
	Declare Function Create(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal hParent As HXCGUI) As HELE'XC_API HELE WINAPI XEditSet_Create(int x, int y, int cx, int cy, HXCGUI hParent);	
	
Public:
	Declare Property hEleHandle()As HELE
	Declare Property hEleHandle(ByVal hEle As HELE)
End Type

Type clsXEditFile Extends Object
Private:    
    m_hEle As HELE
Public:
	Declare Function Create(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal hParent As HXCGUI = NULL) As HELE'XC_API HELE WINAPI XEditFile_Create(int x, int y, int cx, int cy, HXCGUI hParent = NULL);
	Declare Sub SetOpenFileType(ByVal pType As LPCWSTR)'XC_API void WINAPI XEditFile_SetOpenFileType(HELE hEle, const wchar_t* pType); //设置打开文件类型
	Declare Sub SetDefaultFile(ByVal pFile As LPCWSTR)'XC_API void WINAPI XEditFile_SetDefaultFile(HELE hEle, const wchar_t* pFile); //设置默认目录文件
	Declare Sub SetRelativeDir(ByVal pDir As LPCWSTR)'XC_API void WINAPI XEditFile_SetRelativeDir(HELE hEle, const wchar_t* pDir); //TODO:设置相对路径	
	
Public:
	Declare Property hEleHandle()As HELE
	Declare Property hEleHandle(ByVal hEle As HELE)
End Type

Type clsXEditFolder Extends Object
Private:    
    m_hEle As HELE
Public:
	Declare Function Create(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal hParent As HXCGUI) As HELE'XC_API HELE WINAPI XEditFolder_Create(int x, int y, int cx, int cy, HXCGUI hParent);
	Declare Sub SetDefaultDir(ByVal pDir As LPCWSTR)'XC_API void WINAPI XEditFolder_SetDefaultDir(HELE hEle, const wchar_t* pDir);  //设置默认目录	
	
Public:
	Declare Property hEleHandle()As HELE
	Declare Property hEleHandle(ByVal hEle As HELE)
End Type


#endif
