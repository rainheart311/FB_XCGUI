#define UNICODE
#include Once "windows.bi"
#include Once "clsXCGUI.bi"
#include Once "clsXButton.bi"
#include Once "clsXWnd.bi"
#include Once "clsXEditor.bi"
#include Once "clsXFont.bi"

Dim Shared xc As clsXCGUI
Dim Shared frmMain As clsXWnd
Dim Shared btnClose As clsXButton
Dim Shared txtEditor As clsXEditor
Dim Shared xcFont As clsXFont

Function WinMain(ByVal hInst As HINSTANCE,ByVal hPrevInst As HINSTANCE,ByVal szCmdLine As ZString Ptr,ByVal nCmdShow As Long) As Long
	xc.XInitXCGUI(TRUE)
	frmMain.Create(0,0,800,500,"�Ųʽ���ⴰ��-3.3.5")
	Print "���ھ��",frmMain.hWndHandle
	btnClose.Create(350,5,60,20,"",frmMain.hWndHandle)
	btnClose.SetTypeEx(XC_OBJECT_TYPE_EX.button_type_close)
	
	txtEditor.Create(20,40,750,450,frmMain.hWndHandle)
	Print "���ھ��",txtEditor.hEleHandle
	txtEditor.EnableKeyTab(TRUE)
	txtEditor.EnableAutoWrap(TRUE)
	
	txtEditor.SetFont(xcFont.CreateEx("΢���ź�",12))
	txtEditor.SetTextColor(COLORREF_MAKE(100,100,100,255)) '//Ĭ����ɫ
    
    txtEditor.SetStyleMacro(txtEditor.AddStyle(NULL,COLORREF_MAKE(189,99,179,255),TRUE))		'//���� 
    txtEditor.SetStyleFunction(txtEditor.AddStyle(NULL,COLORREF_MAKE(255,128,0,255),TRUE))		'//����
    txtEditor.SetStyleString(txtEditor.AddStyle(NULL,COLORREF_MAKE(206,145,120,255),TRUE))		'//�ַ��� 
	txtEditor.SetStyleComment(txtEditor.AddStyle(NULL,COLORREF_MAKE(67,166,74,255),TRUE))		'//ע��
 
    Dim iStyle_key1 As Long = txtEditor.AddStyle(NULL,COLORREF_MAKE(86, 156, 214,255), True)    '//key1 
    Dim iStyle_key2 As Long = txtEditor.AddStyle(NULL,COLORREF_MAKE(200, 0, 0,255), True)       '//key2
    
    txtEditor.AddKeyword("if",iStyle_key1)
    txtEditor.AddKeyword("int",iStyle_key1)
    txtEditor.AddKeyword("function",iStyle_key2)
    txtEditor.AddKeyword("return",iStyle_key2)
    
    txtEditor.AddConst("XE_BNCLICK //��ť����¼�")
    
    txtEditor.AddFunction("HXCGUI XC_LoadLayout(const wchar_t *pFileName, HXCGUI hParent=NULL); //��������")
    txtEditor.AddFunction("HXCGUI XEle_RegEvent(const wchar_t *pFileName, HXCGUI hParent=NULL); //��������")
    txtEditor.AddFunction("HXCGUI XWnd_AdjustLayout(const wchar_t *pFileName, HXCGUI hParent=NULL); //��������")
    txtEditor.AddFunction("HXCGUI XWnd_ShowWindow(const wchar_t *pFileName, HXCGUI hParent=NULL); //��������")
   
	txtEditor.SetBreakpoint(1,TRUE)
	txtEditor.SetBreakpoint(2,TRUE)
	txtEditor.SetBreakpoint(3,FALSE)
    txtEditor.SetRunRow(0)
    
    Dim s As WString * 1024
    s = "int main(int a, int b) //123456" & Chr(13,10)
    s &= "{" & Chr(13,10)
    s &= "    XC_LoadLayout(""layout.xml"",0);" & Chr(13,10)
    s &= "    XE_BNCLICK;" & Chr(13,10)
    s &= "    return 0;" & Chr(13,10)
    s &= "}" & Chr(13,10)
    txtEditor.SetText(@s)

	frmMain.ShowWindow(SW_SHOW)
	xc.XRunXCGUI
    xc.XExitXCGUI 
	Function = 0
End Function

'{ program start
#ifndef __FB_64BIT__
	pLib = DyLibLoad("xcgui32.dll") 
#else
	pLib = DyLibLoad("xcgui64.dll") 
#endif
	If pLib Then 
		WinMain(GetModuleHandle(NULL),NULL,Command(),SW_NORMAL)
		DyLibFree(pLib)
	Else 
		Print "DLL������"
	EndIf
'}