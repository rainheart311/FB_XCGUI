#include Once "windows.bi"
#include Once "clsXFont.bi"

'XC_API HFONTX WINAPI XFont_CreateEx(const wchar_t* pName = L"����", int size = 12, int style = fontStyle_regular);
Function clsXFont.CreateEx(ByVal pName As LPCWSTR,ByVal nSize As Long = 12,ByVal iStyle As Long) As HFONTX
	If pLib Then   
        Dim pFunc As Function(ByVal As LPCWSTR,ByVal As Long,ByVal As Long) As HELE
        pFunc = DyLibSymbol(pLib,"XFont_CreateEx") 
        If pFunc Then 
            m_hFont = pFunc(pName,nSize,iStyle)
        End If
	End If
	Return m_hFont
End Function

Property clsXFont.hFontHandle()As HFONTX
	Return m_hFont
End Property

Property clsXFont.hFontHandle(ByVal hFont As HFONTX)
	m_hFont = hFont
End Property

