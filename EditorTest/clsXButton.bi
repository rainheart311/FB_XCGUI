#ifndef __CLSXBUTTON_BI__
#define __CLSXBUTTON_BI__

#include Once "XCGUI.bi"


Type clsXButton Extends Object 
Private:    
    m_hEle As HELE = NULL 
Public: 
	Declare Function Create(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal pName As LPCWSTR,ByVal hParent As HXCGUI = NULL) As HELE'XC_API HELE WINAPI XBtn_Create(int x, int y, int cx, int cy, const wchar_t* pName, HXCGUI hParent = NULL);
	Declare Function IsCheck() As BOOL'XC_API BOOL WINAPI XBtn_IsCheck(HELE hEle); //按钮是否被选中
	Declare Function SetCheck(ByVal bCheck As BOOL) As BOOL'XC_API BOOL WINAPI XBtn_SetCheck(HELE hEle, BOOL bCheck);	
	Declare Sub SetState(ByVal nState As common_state3_)'XC_API void WINAPI XBtn_SetState(HELE hEle, common_state3_ nState);
	Declare Function GetState() As common_state3_'XC_API common_state3_  WINAPI XBtn_GetState(HELE hEle);
	Declare Function GetStateEx() As button_state_'XC_API button_state_   WINAPI XBtn_GetStateEx(HELE hEle); 
	Declare Sub SetTypeEx(ByVal nType As XC_OBJECT_TYPE_EX)'XC_API void WINAPI XBtn_SetTypeEx(HELE hEle, XC_OBJECT_TYPE_EX nType);
	Declare Sub SetGroupID(ByVal nID As Long)'XC_API void WINAPI XBtn_SetGroupID(HELE hEle, int nID);
	Declare Function GetGroupID() As Long'XC_API int  WINAPI XBtn_GetGroupID(HELE hEle);
	Declare Sub SetBindEle(ByVal hBindEle As HELE)'XC_API void WINAPI XBtn_SetBindEle(HELE hEle, HELE hBindEle);
	Declare Function GetBindEle() As HELE'XC_API HELE WINAPI XBtn_GetBindEle(HELE hEle);
	Declare Sub SetTextAlign(ByVal nFlags As Long)'XC_API void WINAPI XBtn_SetTextAlign(HELE hEle, int nFlags);
	Declare Function GetTextAlign() As Long'XC_API int  WINAPI XBtn_GetTextAlign(HELE hEle);
	Declare Sub SetIconAlign(ByVal align As button_icon_align_)'XC_API void WINAPI XBtn_SetIconAlign(HELE hEle, button_icon_align_ align);
	Declare Sub SetOffset(ByVal x As Long,ByVal y As Long)'XC_API void WINAPI XBtn_SetOffset(HELE hEle, int x, int y);
	Declare Sub SetOffsetIcon(ByVal x As Long,ByVal y As Long)'XC_API void WINAPI XBtn_SetOffsetIcon(HELE hEle, int x, int y);
	Declare Sub SetIconSpace(ByVal nSize As Long)'XC_API void WINAPI XBtn_SetIconSpace(HELE hEle, int size);
	Declare Sub SetText(ByVal pName As LPCWSTR)'XC_API void WINAPI XBtn_SetText(HELE hEle, const wchar_t* pName);
	Declare Function GetText() As LPCWSTR'XC_API const wchar_t* WINAPI XBtn_GetText(HELE hEle);
	Declare Sub SetIcon(ByVal hImage As HIMAGE)'XC_API void WINAPI XBtn_SetIcon(HELE hEle, HIMAGE hImage);
	Declare Sub SetIconDisable(ByVal hImage As HIMAGE)'XC_API void WINAPI XBtn_SetIconDisable(HELE hEle, HIMAGE hImage);
	Declare Function GetIcon(ByVal nType As Long) As HIMAGE'XC_API HIMAGE WINAPI XBtn_GetIcon(HELE hEle, int nType);
	Declare Sub AddAnimationFrame(ByVal hImage As HIMAGE,ByVal uElapse As UINT)'XC_API void WINAPI XBtn_AddAnimationFrame(HELE hEle, HIMAGE hImage, UINT uElapse);
	Declare Sub EnableAnimation(ByVal bEnable As BOOL,ByVal bLoopPlay As BOOL = FALSE)'XC_API void WINAPI XBtn_EnableAnimation(HELE hEle, BOOL bEnable, BOOL bLoopPlay = FALSE); 
End Type









#endif





