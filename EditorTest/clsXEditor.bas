#include Once "windows.bi"
#include Once "clsXEditor.bi"

'XC_API HELE WINAPI XEditor_Create(int x, int y, int cx, int cy, HXCGUI hParent = NULL);
Function clsXEditor.Create(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal hParent As HXCGUI = NULL) As HELE
	If pLib Then   
        Dim pFunc As Function(ByVal As Long,ByVal As Long,ByVal As Long,ByVal As Long,ByVal As HXCGUI) As HELE
        pFunc = DyLibSymbol(pLib,"XEditor_Create") 
        If pFunc Then 
            m_hEle = pFunc(x,y,cx,cy,hParent)
        Else
    		Print "XEditor_Create函数不存在"
        End If
	Else
    	Print "DLL不存在"
	End If
	Return m_hEle
End Function

'XC_API BOOL WINAPI XEditor_SetBreakpoint(HELE hEle, int iRow, BOOL bActivate = TRUE);
Function clsXEditor.SetBreakpoint(ByVal iRow As Long,ByVal bActivate As BOOL = TRUE) As BOOL
	If pLib Then   
        Dim pFunc As Function(ByVal As HELE,ByVal As Long,ByVal As BOOL) As BOOL  
        pFunc = DyLibSymbol(pLib,"XEditor_SetBreakpoint") 
        If pFunc Then 
            Return pFunc(m_hEle,iRow,bActivate)
        Else
    		Print "XEditor_SetBreakpoint函数不存在"
        End If
	Else
    	Print "DLL不存在"
    End If
End Function

'XC_API BOOL WINAPI XEditor_SetRunRow(HELE hEle, int iRow);
Function clsXEditor.SetRunRow(ByVal iRow As Long) As BOOL
	If pLib Then   
        Dim pFunc As Function(ByVal As HELE,ByVal As Long) As BOOL  
        pFunc = DyLibSymbol(pLib,"XEditor_SetRunRow") 
        If pFunc Then 
            Return pFunc(m_hEle,iRow)
        Else
    		Print "XEditor_SetRunRow函数不存在"
        End If
	Else
    	Print "DLL不存在"
    End If
End Function

'XC_API void WINAPI XEditor_SetStyleFunction(HELE hEle, int iStyle);
Sub clsXEditor.SetStyleFunction(ByVal iStyle As Long)
    If pLib Then   
        Dim pFunc As Sub(ByVal As HELE,ByVal As Long)
        pFunc = DyLibSymbol(pLib,"XEditor_SetStyleFunction") 
        If pFunc Then 
            pFunc(m_hEle,iStyle)
        Else
    		Print "XEditor_SetStyleFunction函数不存在"
        End If
    Else
    	Print "DLL不存在"
    End If
End Sub

'XC_API void WINAPI XEditor_SetStyleMacro(HELE hEle, int iStyle);
Sub clsXEditor.SetStyleMacro(ByVal iStyle As Long)
	If pLib Then   
        Dim pFunc As Sub(ByVal As HELE,ByVal As Long)
        pFunc = DyLibSymbol(pLib,"XEditor_SetStyleMacro") 
        If pFunc Then 
            pFunc(m_hEle,iStyle)
        Else
    		Print "XEditor_SetStyleMacro函数不存在"
        End If
	Else
    	Print "DLL不存在"            
    End If  
End Sub

'XC_API void WINAPI XEditor_SetStyleString(HELE hEle, int iStyle);
Sub clsXEditor.SetStyleString(ByVal iStyle As Long)
	If pLib Then   
        Dim pFunc As Sub(ByVal As HELE,ByVal As Long)
        pFunc = DyLibSymbol(pLib,"XEditor_SetStyleString") 
        If pFunc Then 
            pFunc(m_hEle,iStyle)
        Else
    		Print "XEditor_SetStyleString函数不存在"
        End If
	Else
    	Print "DLL不存在"
    End If  
End Sub

'XC_API void WINAPI XEditor_SetStyleComment(HELE hEle, int iStyle);
Sub clsXEditor.SetStyleComment(ByVal iStyle As Long)
	If pLib Then   
        Dim pFunc As Sub(ByVal As HELE,ByVal As Long)
        pFunc = DyLibSymbol(pLib,"XEditor_SetStyleComment") 
        If pFunc Then 
            pFunc(m_hEle,iStyle)
        Else
    		Print "XEditor_SetStyleComment函数不存在"
        End If
	Else
    	Print "DLL不存在"
    End If
End Sub

'XC_API void WINAPI XEditor_AddKeyword(HELE hEle, const wchar_t* pKey, int iStyle);
Sub clsXEditor.AddKeyword(ByVal pKey As LPCWSTR,ByVal iStyle As Long)
	If pLib Then   
        Dim pFunc As Sub(ByVal As HELE,ByVal As LPCWSTR,ByVal As Long)
        pFunc = DyLibSymbol(pLib,"XEditor_AddKeyword") 
        If pFunc Then 
            pFunc(m_hEle,pKey,iStyle)
        Else
    		Print "XEditor_AddKeyword函数不存在"
        End If
	Else
    	Print "DLL不存在"
    End If    
End Sub

'XC_API void WINAPI XEditor_AddConst(HELE hEle, const wchar_t* pKey);
Sub clsXEditor.AddConst(ByVal pKey As LPCWSTR)
	If pLib Then   
        Dim pFunc As Sub(ByVal As HELE,ByVal As LPCWSTR)
        pFunc = DyLibSymbol(pLib,"XEditor_AddConst") 
        If pFunc Then 
            pFunc(m_hEle,pKey)
        Else
    		Print "XEditor_AddConst函数不存在"
        End If
	Else
    	Print "DLL不存在"
    End If   
End Sub

'XC_API void WINAPI XEditor_AddFunction(HELE hEle, const wchar_t* pKey);
Sub clsXEditor.AddFunction(ByVal pKey As LPCWSTR)
	If pLib Then   
        Dim pFunc As Sub(ByVal As HELE,ByVal As LPCWSTR)
        pFunc = DyLibSymbol(pLib,"XEditor_AddFunction") 
        If pFunc Then 
            pFunc(m_hEle,pKey)
        Else
    		Print "XEditor_AddFunction函数不存在"
        End If
	Else
    	Print "DLL不存在"
    End If
End Sub

