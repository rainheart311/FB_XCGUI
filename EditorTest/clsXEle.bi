#ifndef __CLSXELE_BI__
#define __CLSXELE_BI__

#include Once "XCGUI.bi"

Type clsXEle Extends Object 
Protected:
    m_hEle As HELE
Public:
	Declare Function Create(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal hParent As HXCGUI = NULL) As HELE'XC_API HELE WINAPI XEle_Create(int x, int y, int cx, int cy, HXCGUI hParent = NULL);
	Declare Function RegEventC(ByVal nEvent As Long,ByVal pFun As Any Ptr) As BOOL'XC_API BOOL WINAPI XEle_RegEventC(HELE hEle, int nEvent, void* pFun); //注册事件C方式
	Declare Function RegEventC1(ByVal nEvent As Long,ByVal pFun As Any Ptr) As BOOL'XC_API BOOL WINAPI XEle_RegEventC1(HELE hEle, int nEvent, void* pFun);
	Declare Function RegEventC2(ByVal nEvent As Long,ByVal pFun As Any Ptr) As BOOL'XC_API BOOL WINAPI XEle_RegEventC2(HELE hEle, int nEvent, void* pFun);
	Declare Function RemoveEventC(ByVal nEvent As Long,ByVal pFun As Any Ptr) As BOOL'XC_API BOOL WINAPI XEle_RemoveEventC(HELE hEle, int nEvent, void* pFun); //移除事件函数C方式
	Declare Function SendEvent(ByVal nEvent As Long,ByVal wParam As WPARAM,ByVal lParam As LPARAM) As Long'XC_API int  WINAPI XEle_SendEvent(HELE hEle, int nEvent, WPARAM wParam, LPARAM lParam);
	Declare Function PostEvent(ByVal nEvent As Long,ByVal wParam As WPARAM,ByVal lParam As LPARAM) As BOOL'XC_API BOOL WINAPI XEle_PostEvent(HELE hEle, int nEvent, WPARAM wParam, LPARAM lParam);
	Declare Function IsShow() As BOOL'XC_API BOOL WINAPI XEle_IsShow(HELE hEle);
	Declare Function IsEnable() As BOOL'XC_API BOOL WINAPI XEle_IsEnable(HELE hEle);
	Declare Function IsEnableFocus() As BOOL'XC_API BOOL WINAPI XEle_IsEnableFocus(HELE hEle);
	Declare Function IsDrawFocus() As BOOL'XC_API BOOL WINAPI XEle_IsDrawFocus(HELE hEle);
	Declare Function IsEnableEvent_XE_PAINT_END() As BOOL'XC_API BOOL WINAPI XEle_IsEnableEvent_XE_PAINT_END(HELE hEle);
	Declare Function IsMouseThrough() As BOOL'XC_API BOOL WINAPI XEle_IsMouseThrough(HELE hEle);
	Declare Function IsBkTransparent() As BOOL'XC_API BOOL WINAPI XEle_IsBkTransparent(HELE hEle);
	Declare Function IsKeyTab() As BOOL'XC_API BOOL WINAPI XEle_IsKeyTab(HELE hEle);
	Declare Function IsSwitchFocus() As BOOL'XC_API BOOL WINAPI XEle_IsSwitchFocus(HELE hEle);
	Declare Function IsEnable_XE_MOUSEWHEEL() As BOOL'XC_API BOOL WINAPI XEle_IsEnable_XE_MOUSEWHEEL(HELE hEle);
	Declare Function IsChildEle(ByVal hChildEle As HELE) As BOOL'XC_API BOOL WINAPI XEle_IsChildEle(HELE hEle, HELE hChildEle);
	Declare Function IsEnableCanvas() As BOOL'XC_API BOOL WINAPI XEle_IsEnableCanvas(HELE hEle);
	Declare Function IsFocus() As BOOL'XC_API BOOL WINAPI XEle_IsFocus(HELE hEle);
	Declare Function IsFocusEx() As BOOL'XC_API BOOL WINAPI XEle_IsFocusEx(HELE hEle);
	Declare Sub Enable(ByVal bEnable As BOOL)'XC_API void WINAPI XEle_Enable(HELE hEle, BOOL bEnable); 
	Declare Sub EnableFocus(ByVal bEnable As BOOL)'XC_API void WINAPI XEle_EnableFocus(HELE hEle, BOOL bEnable);
	Declare Sub EnableDrawFocus(ByVal bEnable As BOOL)'XC_API void WINAPI XEle_EnableDrawFocus(HELE hEle, BOOL bEnable);
	Declare Sub EnableDrawBorder(ByVal bEnable As BOOL)'XC_API void WINAPI XEle_EnableDrawBorder(HELE hEle, BOOL bEnable);
	Declare Sub EnableCanvas(ByVal bEnable As BOOL)'XC_API void WINAPI XEle_EnableCanvas(HELE hEle, BOOL bEnable);
	Declare Sub EnableBkTransparent(ByVal bEnable As BOOL)'XC_API void WINAPI XEle_EnableBkTransparent(HELE hEle, BOOL bEnable);
	Declare Sub EnableMouseThrough(ByVal bEnable As BOOL)'XC_API void WINAPI XEle_EnableMouseThrough(HELE hEle, BOOL bEnable);
	Declare Sub EnableKeyTab(ByVal bEnable As BOOL)'XC_API void WINAPI XEle_EnableKeyTab(HELE hEle, BOOL bEnable);
	Declare Sub EnableSwitchFocus(ByVal bEnable As BOOL)'XC_API void WINAPI XEle_EnableSwitchFocus(HELE hEle, BOOL bEnable);
	Declare Sub EnableEvent_XE_PAINT_END(ByVal bEnable As BOOL)'XC_API void WINAPI XEle_EnableEvent_XE_PAINT_END(HELE hEle, BOOL bEnable);
	Declare Sub EnableEvent_XE_MOUSEWHEEL(ByVal bEnable As BOOL)'XC_API void WINAPI XEle_EnableEvent_XE_MOUSEWHEEL(HELE hEle, BOOL bEnable);
	Declare Function SetRect(ByVal pRect As LPRECT,ByVal bRedraw As BOOL = FALSE,ByVal nFlags As Long = adjustLayout_all,ByVal nAdjustNo As UINT = 0) As Long'XC_API int  WINAPI XEle_SetRect(HELE hEle, RECT* pRect, BOOL bRedraw = FALSE, int nFlags = adjustLayout_all, UINT nAdjustNo = 0);
	Declare Function SetRectEx(ByVal x As Long,ByVal y As Long,ByVal cx As Long,ByVal cy As Long,ByVal bRedraw As BOOL = FALSE,ByVal nFlags As Long = adjustLayout_all,ByVal nAdjustNo As UINT = 0) As Long'XC_API int  WINAPI XEle_SetRectEx(HELE hEle, int x, int y, int cx, int cy, BOOL bRedraw = FALSE, int nFlags = adjustLayout_all, UINT nAdjustNo = 0);
	Declare Function SetRectLogic(ByVal pRect As LPRECT,ByVal bRedraw As BOOL = FALSE,ByVal nFlags As Long = adjustLayout_all,ByVal nAdjustNo As UINT = 0) As Long'XC_API int  WINAPI XEle_SetRectLogic(HELE hEle, RECT* pRect, BOOL bRedraw = FALSE, int nFlags = adjustLayout_all, UINT nAdjustNo = 0); //逻辑模式坐标
	Declare Sub GetRect(ByRef pRect As RECT)'XC_API void WINAPI XEle_GetRect(HELE hEle, out_ RECT* pRect);   //相对与父坐标,人眼观察模式
	Declare Sub GetRectLogic(ByRef pRect As RECT)'XC_API void WINAPI XEle_GetRectLogic(HELE hEle, out_ RECT* pRect); //相对与父坐标,逻辑模式
	Declare Sub GetClientRect(ByRef pRect As RECT)'XC_API void WINAPI XEle_GetClientRect(HELE hEle, out_ RECT* pRect);  //左上角为0，0坐标
	Declare Sub GetWndClientRect(ByRef pRect As RECT)'XC_API void WINAPI XEle_GetWndClientRect(HELE hEle, out_ RECT* pRect);
	Declare Sub SetWidth(ByVal nWidth As Long)'XC_API void WINAPI XEle_SetWidth(HELE hEle, int nWidth);
	Declare Sub SetHeight(ByVal nHeight As Long)'XC_API void WINAPI XEle_SetHeight(HELE hEle, int nHeight);
	Declare Function GetWidth() As Long'XC_API int  WINAPI XEle_GetWidth(HELE hEle);
	Declare Function GetHeight() As Long'XC_API int  WINAPI XEle_GetHeight(HELE hEle);
	Declare Sub RectWndClientToEleClient(ByRef pRect As RECT)'XC_API void WINAPI XEle_RectWndClientToEleClient(HELE hEle, in_out_ RECT* pRect);
	Declare Sub PointWndClientToEleClient(ByRef pt As Point)'XC_API void WINAPI XEle_PointWndClientToEleClient(HELE hEle, in_out_ POINT* pPt);
	Declare Sub RectClientToWndClient(ByRef pRect As RECT)'XC_API void WINAPI XEle_RectClientToWndClient(HELE hEle, in_out_ RECT* pRect);
	Declare Sub PointClientToWndClient(ByRef pt As Point)'XC_API void WINAPI XEle_PointClientToWndClient(HELE hEle, in_out_ POINT* pPt);
	Declare Function AddChild(ByVal hChild As HXCGUI) As BOOL'XC_API BOOL WINAPI XEle_AddChild(HELE hEle, HXCGUI hChild);
	Declare Function InsertChild(ByVal hChild As HXCGUI,ByVal index As Long) As BOOL'XC_API BOOL WINAPI XEle_InsertChild(HELE hEle, HXCGUI hChild, int index);  //插入到指定位置
	Declare Sub Remove()'XC_API void WINAPI XEle_Remove(HELE hEle);
	Declare Function SetZOrder(ByVal index As Long) As BOOL'XC_API BOOL WINAPI XEle_SetZOrder(HELE hEle, int index);
	Declare Function SetZOrderEx(ByVal hDestEle As HELE,ByVal nType As zorder_) As BOOL'XC_API BOOL WINAPI XEle_SetZOrderEx(HELE hEle, HELE hDestEle, zorder_ nType);
	Declare Function GetZOrder() As Long'XC_API int  WINAPI XEle_GetZOrder(HELE hEle);
	Declare Function EnableTopmost(ByVal bTopmost As BOOL) As BOOL'XC_API BOOL WINAPI XEle_EnableTopmost(HELE hEle, BOOL bTopmost);
	Declare Sub SetCursor(ByVal hCur As HCURSOR)'XC_API void WINAPI XEle_SetCursor(HELE hEle, HCURSOR SetCursor);
	Declare Function GetCursor() As HCURSOR'XC_API HCURSOR WINAPI XEle_GetCursor(HELE hEle);
	Declare Sub SetTextColor(ByVal nColor As COLORREF)'XC_API void WINAPI XEle_SetTextColor(HELE hEle, COLORREF color); 
	Declare Function GetTextColor() As COLORREF'XC_API COLORREF WINAPI XEle_GetTextColor(HELE hEle);
	Declare Function GetTextColorEx() As COLORREF'XC_API COLORREF WINAPI XEle_GetTextColorEx(HELE hEle);
	Declare Sub SetFocusBorderColor(ByVal nColor As COLORREF)'XC_API void WINAPI XEle_SetFocusBorderColor(HELE hEle, COLORREF color);
	Declare Function GetFocusBorderColor() As COLORREF'XC_API COLORREF WINAPI XEle_GetFocusBorderColor(HELE hEle);
	Declare Sub SetFont(ByVal hFont As HFONTX)'XC_API void WINAPI XEle_SetFont(HELE hEle, HFONTX hFontx);
	Declare Function GetFont() As HFONTX'XC_API HFONTX WINAPI XEle_GetFont(HELE hEle);
	Declare Function GetFontEx() As HFONTX'XC_API HFONTX WINAPI XEle_GetFontEx(HELE hEle);
	Declare Sub SetAlpha(ByVal nAlpha As UByte)'XC_API void WINAPI XEle_SetAlpha(HELE hEle, BYTE alpha);
	Declare Function GetChildCount() As Long'XC_API int  WINAPI XEle_GetChildCount(HELE hEle);
	Declare Function GetChildByIndex(ByVal index As Long) As HXCGUI'XC_API HXCGUI WINAPI XEle_GetChildByIndex(HELE hEle, int index);
	Declare Function GetChildByID(ByVal nID As Long) As HXCGUI'XC_API HXCGUI WINAPI XEle_GetChildByID(HELE hEle, int nID);
	Declare Sub SetBorderSize(ByVal nLeft As Long,ByVal nTop As Long,ByVal nRight As Long,ByVal nBottom As Long)'XC_API void WINAPI XEle_SetBorderSize(HELE hEle, int left, int top, int right, int bottom);
	Declare Sub GetBorderSize(ByRef pBorder As borderSize_)'XC_API void WINAPI XEle_GetBorderSize(HELE hEle, out_ borderSize_* pBorder);
	Declare Sub SetPadding(ByVal nLeft As Long,ByVal nTop As Long,ByVal nRight As Long,ByVal nBottom As Long)'XC_API void WINAPI XEle_SetPadding(HELE hEle, int left, int top, int right, int bottom);
	Declare Sub GetPadding(ByRef pPadding As paddingSize_)'XC_API void WINAPI XEle_GetPadding(HELE hEle, out_ paddingSize_* pPadding);
	Declare Sub SetDragBorder(ByVal nFlags As Long)'XC_API void WINAPI XEle_SetDragBorder(HELE hEle, int nFlags);
	Declare Sub SetDragBorderBindEle(ByVal nFlags As Long,ByVal hBindEle As HELE,ByVal nSpace As Long)'XC_API void WINAPI XEle_SetDragBorderBindEle(HELE hEle, int nFlags, HELE hBindEle, int nSpace);
	Declare Sub SetMinSize(ByVal nWidth As Long,ByVal nHeight As Long)'XC_API void WINAPI XEle_SetMinSize(HELE hEle, int nWidth, int nHeight);
	Declare Sub SetMaxSize(ByVal nWidth As Long,ByVal nHeight As Long)'XC_API void WINAPI XEle_SetMaxSize(HELE hEle, int nWidth, int nHeight);
	Declare Sub SetLockScroll(ByVal bHorizon As BOOL,ByVal bVertical As BOOL)'XC_API void WINAPI XEle_SetLockScroll(HELE hEle, BOOL bHorizon, BOOL bVertical);
	Declare Function HitChildEle(ByVal lppt As LPPOINT) As HELE'XC_API HELE WINAPI XEle_HitChildEle(HELE hEle, in_ POINT* pPt);
	Declare Sub SetUserData(ByVal nData As Integer)'XC_API void WINAPI XEle_SetUserData(HELE hEle, vint nData);
	Declare Function GetUserData() As Integer'XC_API vint WINAPI XEle_GetUserData(HELE hEle);
	Declare Sub GetContentSize(ByVal bHorizon As BOOL,ByVal cx As Long,ByVal cy As Long,ByRef pSize As SIZE)'XC_API void WINAPI XEle_GetContentSize(HELE hEle, BOOL bHorizon, int cx, int cy, out_ SIZE* pSize);
	Declare Sub SetCapture(ByVal b As BOOL)'XC_API void WINAPI XEle_SetCapture(HELE hEle, BOOL b);
	Declare Sub Redraw(ByVal bImmediate As BOOL = FALSE)'XC_API void WINAPI XEle_Redraw(HELE hEle, BOOL bImmediate = FALSE);
	Declare Sub RedrawRect(ByVal pRect As LPRECT,ByVal bImmediate As BOOL = FALSE)'XC_API void WINAPI XEle_RedrawRect(HELE hEle, RECT* pRect, BOOL bImmediate = FALSE);
	Declare Sub Destroy()'XC_API void WINAPI XEle_Destroy(HELE hEle);  //销毁
	Declare Sub AddBkBorder(ByVal nState As Long,ByVal nColor As COLORREF,ByVal nWidth As Long)'XC_API void WINAPI XEle_AddBkBorder(HELE hEle,int nState, COLORREF color, int width);
	Declare Sub AddBkFill(ByVal nState As Long,ByVal nColor As COLORREF)'XC_API void WINAPI XEle_AddBkFill(HELE hEle, int nState, COLORREF color);
	Declare Sub AddBkImage(ByVal nState As Long,ByVal hImage As HIMAGE)'XC_API void WINAPI XEle_AddBkImage(HELE hEle, int nState, HIMAGE hImage);
	Declare Function GetBkInfoCount() As Long'XC_API int  WINAPI XEle_GetBkInfoCount(HELE hEle);
	Declare Sub ClearBkInfo()'XC_API void WINAPI XEle_ClearBkInfo(HELE hEle);
	Declare Function GetBkManager() As HBKM'XC_API HBKM WINAPI XEle_GetBkManager(HELE hEle);
	Declare Function GetBkManagerEx() As HBKM'XC_API HBKM WINAPI XEle_GetBkManagerEx(HELE hEle);
	Declare Sub SetBkManager(ByVal hBkInfoM As HBKM)'XC_API void WINAPI XEle_SetBkManager(HELE hEle, HBKM hBkInfoM);
	Declare Function GetStateFlags() As Long'XC_API int  WINAPI XEle_GetStateFlags(HELE hEle); //获取组合状态
	Declare Function DrawFocus(ByVal hDraw As HDRAW,ByVal pRect As LPRECT) As BOOL'XC_API BOOL WINAPI XEle_DrawFocus(HELE hEle, HDRAW hDraw, RECT* pRect);
	Declare Sub DrawEle(ByVal hDraw As HDRAW)'XC_API void WINAPI XEle_DrawEle(HELE hEle, HDRAW hDraw);
	Declare Sub EnableTransparentChannel(ByVal bEnable As BOOL)'XC_API void WINAPI XEle_EnableTransparentChannel(HELE hEle, BOOL bEnable);
	Declare Function SetXCTimer(ByVal nIDEvent As UINT,ByVal uElapse As UINT) As BOOL'XC_API BOOL WINAPI XEle_SetXCTimer(HELE hEle, UINT nIDEvent, UINT uElapse);
	Declare Function KillXCTimer(ByVal nIDEvent As UINT) As BOOL'XC_API BOOL WINAPI XEle_KillXCTimer(HELE hEle, UINT nIDEvent);
	Declare Sub SetToolTip(ByVal pText As LPCWSTR)'XC_API void WINAPI XEle_SetToolTip(HELE hEle, const wchar_t* pText);
	Declare Sub SetToolTipEx(ByVal pText As LPCWSTR,ByVal nTextAlign As Long)'XC_API void WINAPI XEle_SetToolTipEx(HELE hEle, const wchar_t* pText, int nTextAlign);
	Declare Function GetToolTip() As LPCWSTR'XC_API const wchar_t* WINAPI XEle_GetToolTip(HELE hEle);
	Declare Sub PopupToolTip(ByVal x As Long,ByVal y As Long)'XC_API void WINAPI XEle_PopupToolTip(HELE hEle, int x, int y);
	Declare Sub AdjustLayout(ByVal nAdjustNo As UINT = 0)'XC_API void WINAPI XEle_AdjustLayout(HELE hEle, UINT nAdjustNo = 0);
	Declare Sub AdjustLayoutEx(ByVal nFlags As Long = adjustLayout_self,ByVal nAdjustNo As UINT = 0)'XC_API void WINAPI XEle_AdjustLayoutEx(HELE hEle, int nFlags = adjustLayout_self, UINT nAdjustNo = 0);	
'//3.3.0
	Declare Function SetPosition(ByVal x As Long,ByVal y As Long,ByVal bRedraw As BOOL = FALSE,ByVal nFlags As Long = adjustLayout_all,ByVal nAdjustNo As UINT = 0) As Long'XC_API int  WINAPI XEle_SetPosition(HELE hEle, int x, int y, BOOL bRedraw = FALSE, int nFlags = adjustLayout_all, UINT nAdjustNo = 0);
	Declare Function SetPositionLogic(ByVal x As Long,ByVal y As Long,ByVal bRedraw As BOOL = FALSE,ByVal nFlags As Long = adjustLayout_all,ByVal nAdjustNo As UINT = 0) As Long'XC_API int  WINAPI XEle_SetPositionLogic(HELE hEle, int x, int y, BOOL bRedraw = FALSE, int nFlags = adjustLayout_all, UINT nAdjustNo = 0);
	Declare Sub GetPosition(ByRef pOutX As Long,ByRef pOutY As Long)'XC_API void WINAPI XEle_GetPosition(HELE hEle, int* pOutX, int* pOutY);
	Declare Function SetSize(ByVal nWidth As Long,ByVal nHeight As Long,ByVal bRedraw As BOOL = FALSE,ByVal nFlags As Long = adjustLayout_all,ByVal nAdjustNo As UINT = 0) As Long'XC_API int  WINAPI XEle_SetSize(HELE hEle, int nWidth, int nHeight, BOOL bRedraw = FALSE, int nFlags = adjustLayout_all, UINT nAdjustNo = 0);
	Declare Sub GetSize(ByRef nWidth As Long,ByRef nHeight As Long)'XC_API void WINAPI XEle_GetSize(HELE hEle, out_ int* pOutWidth, out_ int* pOutHeight); 
'//v3.3.2	
	Declare Function SetBkInfo(ByVal pText As LPCWSTR) As Long'XC_API int  WINAPI XEle_SetBkInfo(HELE hEle, const wchar_t* pText);
'Public:
'	Declare Function RegEvent(ByVal nEvent As UINT,ByVal pEvent As xc_event) As BOOL'XC_API BOOL WINAPI _XEle_RegEvent(HELE hEle, UINT nEvent, xc_event* pEvent);
'	Declare Function RemoveEvent(ByVal nEvent As UINT,ByVal pEvent As xc_event) As BOOL'XC_API BOOL WINAPI _XEle_RemoveEvent(HELE hEle, UINT nEvent, xc_event* pEvent);
'	Declare Function RegEvent(ByVal nEvent As UINT,ByVal pEvent As xc_event) As BOOL'XC_API BOOL WINAPI _XEle_RegEvent(HELE hEle, UINT nEvent, xc_event* pEvent);
'	Declare Function RemoveEvent(ByVal nEvent As UINT,ByVal pEvent As xc_event) As BOOL'XC_API BOOL WINAPI _XEle_RemoveEvent(HELE hEle, UINT nEvent, xc_event* pEvent);

Public:
	Declare Property hEleHandle()As HELE
	Declare Property hEleHandle(ByVal hEle As HELE)
End Type

#endif

